#ifndef FUNCTIONSMANAGER_H
#define FUNCTIONSMANAGER_H

#include <QObject>
#include "Structures.h"
#include "G_Function.h"

class G_FunctionsManager : public QObject
{
    Q_OBJECT
public:
    explicit G_FunctionsManager(QObject *parent = 0);


    void RemoveAllConnectorsByPins(G_LineConnector*);
    G_Function* getFunctionActive(){return g_function_active;}
    G_Function* getFunctionById(int);
    G_Pin* getPinActive(){return g_pin_active;}
    G_Function* getFunctionSelected(){return g_function_selected;}
    G_Function* getFunctionSelectedLast(){return g_function_selected_last;} //получить последнюю выделенную функцию
    G_Pin* getPinSelected(){return g_pin_selected;}
    G_Pin* getPinSelectedLast(){return g_pin_selected_last;}

    void FindFunctionsOrPinsByPoint(QPoint);
    void setActiveFunctionPosition(QPoint);
    bool SelectFunction(QGraphicsScene*,QPoint);


    void resetFunctionSelectedLast();
    void resetFunctionSelected(){g_function_selected = 0;}
    void resetPinSelected();
    void resetPinSelectedLast();
    void setPinSelectedLast();
    bool createConnectionPinToPin(G_Pin*& parent, G_Pin*& child);

    bool tryPinsConnect(QPoint point);


    void resetAll();
    void appendFunction(G_Function*);
    void removeFunction(G_Function*);

    QList <G_Function*> getFunctions(){return functions;}

private:
    G_Pin* g_pin_active = 0;
    G_Function* g_function_active = 0;
    G_Function* g_function_selected=0;
    G_Function* g_function_selected_last=0;
    G_Pin* g_pin_selected=0;
    G_Pin* g_pin_selected_last=0;

    QList <G_Function*> functions;  //функции в текущем сценарии

signals:

public slots:
};

#endif // FUNCTIONSMANAGER_H
