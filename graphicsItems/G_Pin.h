#ifndef G_PIN_H
#define G_PIN_H

#include <QObject>
#include <QGraphicsItem>
#include "Structures.h"
#include "G_Lineconnector.h"
#include <QDebug>

class G_Function;

class G_Pin : public QObject
{
    Q_OBJECT
public:

    explicit G_Pin(G_Function*,QObject *parent=0);
    ~G_Pin();

    G_LineConnector* getActiveLineConnector();
    G_Function* getFunction();
    QString getPinValue(){return value;} //возвращает значение заданное по умолчанию
    QString getPinName(){return  name_pin;}
    void setPinName(QString name){ name_pin = name;}
    int getType(){return type;}
    QPointF getPoint() {return point_connector;}


    QRect rect_text;
    QString text;
    QRect rect_selected; //область выделения
    int type;

    Parameter parameter;
   // bool is_selected; //наведение мышкой
    bool is_connector; // true связь между функциями false между переменными
    bool is_connect; //попытка соединения
    bool is_connected = false; //соединен удачно
    QPainterPath path; //внешний вид подключения
    QPointF point_connector;
    bool is_selected = false; //в прямоугольник наведена мышь
    QColor color; //цвет пина

public:
    QPointF addLineConnector();
    void removeActiveLineConnector();
    //bool removeLineConnector(G_LineConnector *line);
    void selectedLineConnector(bool);
    void setPosLineConnectorSelected(QPoint point);
    QList <G_LineConnector*> getLineConnectors();
    void appendLineConnector(G_LineConnector *line);


    void removeLineConnectors();
    void removeLineConnector(G_LineConnector *line_connect_outside);
private:

    QList <G_LineConnector*> lineConnectors;
    G_LineConnector* lineConnectorSelected = 0;
    G_Function *g_function; //родительская функция
    QString name_pin = "none"; //порядковый номер пина в общем списке



    QString value; //значение по умолчанию
   // QList <G_LineConnector*> lineConnectors_out;
};

#endif // G_PIN_H
