#include "G_Pin.h"
#include "G_Function.h"

G_Pin::G_Pin(G_Function* f, QObject *parent): QObject(parent)
{
    g_function = f;
}

G_Pin::~G_Pin()
{
    /*foreach (G_LineConnector* line, lineConnectors) {
        foreach (G_Pin* pin, line->getConnectors()) {
            if (pin){
                QList<G_LineConnector *> connectors = pin->getLineConnectors();
                connectors.removeAll(line);
            }
        }

        delete line;
        line = 0;
    }
    lineConnectors.clear();*/
}

G_LineConnector *G_Pin::getActiveLineConnector()
{
    if (lineConnectorSelected)
    {
        return lineConnectorSelected;
    }
    return 0;
}

G_Function* G_Pin::getFunction(){
    return g_function;
}

//добавление соединения
void G_Pin::appendLineConnector(G_LineConnector *line)
{
    is_connected = true;
    line->setColor(this->color);
    lineConnectors.append(line);
}


//создание соединения
QPointF G_Pin::addLineConnector()
{
    G_LineConnector *line = new G_LineConnector;
    line->setColor(this->color);
    lineConnectorSelected = line;
    lineConnectorSelected->setSelected(false);
    lineConnectors.append(line);
    return this->point_connector;
}

//удаление соединений
//bool G_Pin::removeLineConnector(G_LineConnector* line)
//{
//    bool result = false;
//    foreach (G_LineConnector* line_temp, lineConnectors) {
//        if (line_temp && line_temp == line){
//            lineConnectors.removeAll(line_temp);
//            result = true;
//        }
//        if (!line_temp){
//            lineConnectors.removeAll(line_temp);
//            result = true;
//        }
//    }

//    return result;
//}

//удаление активного выбранного соединения
void G_Pin::removeActiveLineConnector()
{
    if (lineConnectorSelected)
    {
        lineConnectors.removeAll(lineConnectorSelected);
        delete lineConnectorSelected;
        lineConnectorSelected = 0;
    }

}
//изменение конечной позиции текущего соединителя
void G_Pin::setPosLineConnectorSelected(QPoint point)
{
    if (lineConnectorSelected)
    {
         //меняем напрвление сплайна если это вход или выход
        if (this->parameter.parameters_type == "connector_out" || (!this->parameter.parameters_type.contains("connector") && this->parameter.inout_type == 1 ))
            lineConnectorSelected->set_end_pos(point);
        if (this->parameter.parameters_type == "connector_in" || (!this->parameter.parameters_type.contains("connector") && this->parameter.inout_type == 0 ))
            lineConnectorSelected->set_start_pos(point);


    }
}

QList<G_LineConnector *> G_Pin::getLineConnectors()
{
    return lineConnectors;
}

void G_Pin::removeLineConnector(G_LineConnector *line_connect_outside)
{
    if (line_connect_outside){
        foreach (G_LineConnector *line, lineConnectors) {
            if (line_connect_outside == line){
                line = 0;
                this->lineConnectors.removeOne(line);
            }
        }


    }
}

void G_Pin::removeLineConnectors()
{
    foreach (G_LineConnector *line, lineConnectors) {
        /*foreach (G_Pin *pin_connect, line->getConnectors()) { //проходим по всем соединениям пина
            pin_connect->removeLineConnector(line);
        }*/

        if (line){
            delete line;
            line = 0;
        }
    }
    lineConnectors.clear();
}

//сделать первое соединение текущего пина выбранным
void G_Pin::selectedLineConnector(bool flag)
{
    //  if (lineConnectorSelected)
    //      lineConnectorSelected->setSelected(false);
    if (lineConnectors.count()>0){
        lineConnectors.value(0)->setSelected(flag);
        lineConnectorSelected = lineConnectors.value(0);
    }
}
