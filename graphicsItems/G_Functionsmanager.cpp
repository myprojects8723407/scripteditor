#include "G_FunctionsManager.h"

G_FunctionsManager::G_FunctionsManager(QObject *parent) : QObject(parent)
{

}

//-----------------------------очистка всех связей с данным коннектором во всех функциях
void G_FunctionsManager::RemoveAllConnectorsByPins(G_LineConnector *line)
{
    foreach (G_Function* g_function, functions) {
        if (g_function){
            QList <G_Pin*> pins = g_function->getPins();
            foreach (G_Pin* pin, pins) {
                pin->removeLineConnector(line);
                pin->is_connect = false;
                pin->is_connected = false;

            }
        }
    }
    delete line;
    line = 0;
}

G_Function *G_FunctionsManager::getFunctionById(int id)
{
    foreach (G_Function* g_function, functions) {
        if (g_function->getId() == id){
            return g_function;
        }
    }
    return 0;
}

void G_FunctionsManager::setActiveFunctionPosition(QPoint point)
{
    if (g_function_selected)
    {
        g_function_selected->setPos(point.x()-g_function_selected->delta.x(),point.y()-g_function_selected->delta.y());

        QList <G_Pin*> pins = g_function_selected->getPins();
        foreach (G_Pin* pin, pins) {
            QList <G_LineConnector*> line_connectors = pin->getLineConnectors();
            if (pin->parameter.parameters_type == "connector_in" || (!pin->parameter.parameters_type.contains("connector") && pin->parameter.inout_type == 0 ))
            {
                foreach (G_LineConnector* line, line_connectors) {
                    //ставим конец линии в цетр пина
                    if (line)
                        line->set_end_pos(g_function_selected->mapToScene(pin->point_connector));
                }
            }
            if (pin->parameter.parameters_type == "connector_out" || (!pin->parameter.parameters_type.contains("connector") && pin->parameter.inout_type == 1 ))
            {
                foreach (G_LineConnector* line, line_connectors) {
                    //ставим конец линии в цетр пина
                    if (line)
                        line->set_start_pos(g_function_selected->mapToScene(pin->point_connector));
                }
            }
        }
    }

}

//---------------- удаление всех связей последнего выбранного
void G_FunctionsManager::resetPinSelectedLast()
{
    if (g_pin_selected_last){
        g_pin_selected_last->selectedLineConnector(false);
        g_pin_selected_last->is_connect = false;
    }
    // G_LineConnector* line = g_pin_selected_last->getActiveLineConnector();

    //очистка всех связей с данным коннектором во всех функциях
    g_pin_selected_last = 0;
}

//--------------------------- Установка активного пина последним выбранным
void G_FunctionsManager::setPinSelectedLast()
{
    if (g_pin_selected_last){
        //позволяем выделять только выходной пин напрвления выполнения сценария или входной пин переменной так как они имеют не более одного подключения
        if (g_pin_selected_last->parameter.parameters_type == "connector_out" || (g_pin_selected_last->parameter.inout_type == 0 && g_pin_selected_last->parameter.parameters_type != "connector_out"))
            g_pin_selected_last->selectedLineConnector(false);

    }
    g_pin_active->selectedLineConnector(true);
    g_pin_selected_last = g_pin_active; //делаем последним выбранным

}



//----Создание соединения возвращает 2 пина и тип соединения 0 нет соединения 1-соединение функций 2-соединение переменных--------------------------------
bool G_FunctionsManager::createConnectionPinToPin(G_Pin*& parent, G_Pin*& child)
{
    bool result=false;
    if (g_pin_active==g_pin_selected || g_function_active==g_function_selected) {
        if (g_pin_selected)
            g_pin_selected->removeActiveLineConnector();
        return false; //невозможно подключить к одному и тому же пину или в пределах одной функции
    }
    if (g_pin_active && g_pin_selected && g_function_active && g_function_selected && g_pin_active->is_connect){
        //ставим конец линии в цетр пина
        g_pin_active->is_connect = false; //сбрасываем для исключения повторного входа
        g_pin_selected->is_connected = true;
        G_LineConnector *line_connector = g_pin_selected->getActiveLineConnector();
        //line_connector->set_end_pos(g_function_selected->mapToScene(g_pin_active->point_connector));



        //записываем соединенный пин в базу
        if (g_pin_active->parameter.parameters_type == "connector_in" && g_pin_selected->parameter.parameters_type == "connector_out"){ //соеднинение задающее выполнение сценария далее
            child = g_pin_selected;
            parent = g_pin_active;
            line_connector->set_pos(child->getFunction()->mapToScene(child->point_connector),parent->getFunction()->mapToScene(parent->point_connector));
            result = true;
        }
        else if (g_pin_active->parameter.parameters_type == "connector_out" && g_pin_selected->parameter.parameters_type == "connector_in")
        {
            parent = g_pin_selected;
            child = g_pin_active;
            line_connector->set_pos(child->getFunction()->mapToScene(child->point_connector),parent->getFunction()->mapToScene(parent->point_connector));
            result = true;
        }

        else if ((!g_pin_active->parameter.parameters_type.contains("connector") &&
                  g_pin_active->parameter.inout_type == 0 && g_pin_selected->parameter.inout_type  == 1)
                 ){ //соеднинение задающее выполнение сценария далее

            if (g_pin_active->getFunction()->getFunction().comand_name == "Установить"){
                //для правильной установки соединения элементы установить значение глобальной переменой
                child =g_pin_selected;
                parent =  g_pin_active;
                line_connector->set_pos(child->getFunction()->mapToScene(child->point_connector),parent->getFunction()->mapToScene(parent->point_connector));
            }
            else {
                parent = g_pin_selected;
                child = g_pin_active;
                line_connector->set_pos(parent->getFunction()->mapToScene(parent->point_connector),child->getFunction()->mapToScene(child->point_connector));
            }
            result = true;



        }
        else if(!g_pin_active->parameter.parameters_type.contains("connector") && g_pin_active->parameter.inout_type  == 1 && g_pin_selected->parameter.inout_type == 0){

            if (g_pin_selected->getFunction()->getFunction().comand_name == "Установить"){
                //для правильной установки соединения элементы установить значение глобальной переменой
                child =g_pin_active;
                parent =  g_pin_selected;
                line_connector->set_pos(child->getFunction()->mapToScene(child->point_connector),parent->getFunction()->mapToScene(parent->point_connector));

            }
            else {
                child =g_pin_selected;
                parent =  g_pin_active;
                line_connector->set_pos(parent->getFunction()->mapToScene(parent->point_connector),child->getFunction()->mapToScene(child->point_connector));
            }

            result = true;
        }


        g_pin_active->appendLineConnector(line_connector);

        //line_connector->addConnector(child);
        // line_connector->addConnector(parent);
    }
    else{
        //g_pin_selected->is_connect = false;
        //g_pin_selected->is_connected = false;
        if (g_pin_selected)
            g_pin_selected->removeActiveLineConnector();
    }
    //    if ((g_pin_selected && !g_pin_active) || (g_pin_selected && g_pin_active && (g_pin_selected == g_pin_active))
    //            /*|| (functionManager.getPinSelected() && functionManager.getPinActive() && functionManager.getPinActive()->is_connected)*/){ //отменяем выделение если не выбрали или навели не на нужный пин
    //        //если отпустили мышку без соединения с пином или попытались присоединить к тому же пину от которого соединяемся
    //        //удаляем подключение
    //        g_pin_selected->is_connect = false;
    //        g_pin_selected->is_connected = false;
    //        g_pin_selected->removeActiveLineConnector();

    //    }

    return  result;
}

//проверяем возможность соединения пина
//если это пин направления выполнения сценария родительский пин может только одно соединение, дочерний иметь несколько
bool G_FunctionsManager::tryPinsConnect(QPoint point)
{

    //если это выход  направления выполнения сценария и не подключено запрещаем соединение
    if (g_pin_selected && g_pin_selected->parameter.parameters_type.contains("connector_out") && g_pin_selected->is_connected)
    {
        return false;
    }
    //если это вход переменной и подключено запрещаем соединение
    if (g_pin_selected && !g_pin_selected->parameter.parameters_type.contains("connector") && g_pin_selected->parameter.inout_type==0 && g_pin_selected->is_connected)
    {
        return false;
    }

    bool result = false;
    if (g_pin_selected_last){
        //устанавливаем новые координаты свободного конца сплайна
        g_pin_selected_last->setPosLineConnectorSelected(point);
        result = true;
    }

    if (g_pin_selected && g_pin_active && g_pin_selected!=g_pin_active){
        //если пин подключен с обоих концов

        result = false;
        if (g_pin_selected->parameter.parameters_type.contains("connector") && g_pin_active->parameter.parameters_type.contains("connector"))
        {
            //если пытаемся подключить вход к выходу и выход еще не подключен
            if (g_pin_selected->parameter.inout_type == 0 && g_pin_active->parameter.inout_type == 1 && !g_pin_active->is_connected)
            {
                g_pin_active->is_connect = true;
                return true;
            }
            //не подключаем если к пину уже подключено

            //если пытаемся подключить выходу  к входу  то не проверяем что уже подключен
            if (g_pin_selected->parameter.inout_type == 1 && g_pin_active->parameter.inout_type == 0)
            {
                g_pin_active->is_connect = true;
                return true;
            }

        }
        //если это пин передачи переменных, родительский пин может иметь несколько соединений, дочерняя только одно
        if (!g_pin_selected->parameter.parameters_type.contains("connector") && !g_pin_active->parameter.parameters_type.contains("connector"))
        {
            /*if (g_pin_selected->getFunction()->getFunction().comand_name == "Установить" && !g_pin_active->is_connected){
                  g_pin_active->is_connect = true;
                  return true;
             }*/
            //если пытаемся подключить вход к выходу и выход еще не подключен
            if (g_pin_selected->parameter.inout_type == 1 && g_pin_active->parameter.inout_type == 0 && !g_pin_active->is_connected)
            {
                if (g_pin_active->getFunction()->getFunction().comand_name == "Установить" && g_pin_selected->is_connected)
                    //запрещаем множественные соединения для элемента глобальных переменных
                {
                    return false;
                }
                g_pin_active->is_connect = true;
                return true;
            }
            //не подключаем если к пину уже подключено

            //если пытаемся подключить выходу  к входу  то не проверяем что уже подключен
            if (g_pin_selected->parameter.inout_type == 0 && g_pin_active->parameter.inout_type == 1)
            {

                if (g_pin_selected->getFunction()->getFunction().comand_name == "Установить" && g_pin_active->is_connected)
                        //запрещаем множественные соединения для элемента глобальных переменных
                {
                    return false;
                }
                g_pin_active->is_connect = true;
                return true;

            }
        }


    }

    return result;
}

//очистка всего при переключении на другой сценарий
void G_FunctionsManager::resetAll()
{
    functions.clear();
    g_pin_active = 0;
    g_function_active = 0;
    g_function_selected=0;
    g_pin_selected=0;
    g_pin_selected_last=0;
}

void G_FunctionsManager::appendFunction(G_Function *function)
{
    functions.append(function);
}

void G_FunctionsManager::removeFunction(G_Function *function)
{
    foreach (G_Pin *pin, function->getPins()) {
        //        foreach (G_LineConnector *line, pin->getLineConnectors()) {
        //            foreach (G_Pin *p, line->getConnectors()) {
        //                if(p)
        //                {
        //                    foreach (G_LineConnector *l, p->getLineConnectors()) {
        //                        if (line == l)
        //                        {
        //                            l = 0;
        //                            line->removePin(p);
        //                        }
        //                    }


        //                }
        //            }
        //        }

        if (pin->is_connected!=-1)
        {
            foreach (G_Function *function, functions) {
                if (function->getId() == pin->parameter.id_element_connect){

                    foreach (G_Pin *p, function->getPins()) { //ищем связанный пин
                        if(p->parameter.parameters_name == pin->parameter.parameters_name_connect)
                        {
                            foreach (G_LineConnector *l, p->getLineConnectors())
                            { //ищем коннектор
                                foreach (G_LineConnector *line, pin->getLineConnectors())
                                {
                                    if (line == l)
                                    {
                                        l = 0; //обнуляем ссылку на данный пин у связанной функции
                                    }
                                }
                            }
                        }
                    }
                }
            }


            pin->removeLineConnectors();
            delete pin;
            pin = 0;
        }
    }

    functions.removeAll(function);
    delete function;
    if (g_function_active && g_function_active == function) g_function_active = 0;
    if (g_function_selected && g_function_selected == function) g_function_selected = 0;
    if (g_function_selected_last && g_function_selected_last == function) g_function_selected_last = 0;
    function = 0;

}


//----------------------------выбор текущей функции или пина
bool G_FunctionsManager::SelectFunction(QGraphicsScene* scene,QPoint point)
{
    if (g_pin_active){
        if (g_pin_selected == 0) {//запоминаем с какого коннектора начали соединение
            g_function_selected = g_function_active;
            g_pin_selected = g_pin_active;  //запоминаем стартовый коннектор
            g_pin_selected_last = g_pin_active;

            g_pin_selected->is_connect = true;
            if (g_function_active && g_pin_selected){
                g_pin_selected->addLineConnector();
                G_LineConnector *line_connector = g_pin_selected->getActiveLineConnector();
                //ставим конец линии в цетр пина
                //if (this->parameter.parameters_type == "connector_in" || (!this->parameter.parameters_type.contains("connector") && this->parameter.inout_type == 1 ))
                //  line_connector->set_pos(g_function_active->mapToScene(g_pin_selected->point_connector),g_function_active->mapToScene(g_pin_selected->point_connector));
                //if (this->parameter.parameters_type == "connector_out" || (!this->parameter.parameters_type.contains("connector") && this->parameter.inout_type == 0 ))
                line_connector->set_pos(g_function_selected->mapToScene(g_pin_selected->point_connector),g_function_selected->mapToScene(g_pin_selected->point_connector));

                scene->addItem(line_connector);
                scene->update();
            }
            return true;
        }

    }
    else if (/*g_function_selected == 0 &&*/ g_function_active)
    {//запоминаем с какого коннектора начали соединение
        if (g_function_selected_last)
            g_function_selected_last->set_isSelected(false);
        g_function_selected = g_function_active; //для обновления исходящей функции
        g_function_selected_last = g_function_active;
        g_function_selected_last->set_isSelected(true);
        g_function_selected->delta = QPoint(point.x() - g_function_active->scenePos().x(),point.y() - g_function_active->scenePos().y());
        return true;
    }
    else
    {
        resetFunctionSelectedLast();
        resetPinSelectedLast();

    }
}

void G_FunctionsManager::resetFunctionSelectedLast(){
    if (g_function_selected_last)
        g_function_selected_last->set_isSelected(false);
    g_function_selected_last = 0;
}

void G_FunctionsManager::resetPinSelected()
{
    g_pin_selected = 0;
}


//---------------------------- Поиск элемента под курсором -----------------------------------------------
void G_FunctionsManager::FindFunctionsOrPinsByPoint(QPoint point)
{
    G_Pin* g_pin_active_temp = 0;
    G_Function* g_function_active_temp = 0;
    foreach (G_Function* g_function, functions) {
        if (g_function){
            if (g_function->sceneBoundingRect().toRect().contains(point))  //указатель мыши в  прямоугольнике функции
            {
                QList <G_Pin*> pins = g_function->getPins();

                foreach (G_Pin* pin, pins) {
                    QRect rect = g_function->mapRectToScene(pin->rect_selected).toRect(); //преобразуем локальную систему координат item в систему координат scene
                    if (rect.contains(point))
                    {

                        //g_function->setPinSelect(pin,true);
                        g_pin_active_temp = pin;  //мышь наведена на данный контакт
                        continue;
                    }
                }

                g_function_active_temp = g_function;
                functions.removeAt(functions.indexOf(g_function)); //передвигаем текущую функцию на передний план
                functions.push_front(g_function);
                /*if (!g_function->getSelect())
                    g_function->setSelect(true);*/
                break;
            }
        }
    }


    if (g_pin_active_temp){
        if (g_pin_active && g_function_active && g_pin_active_temp!=g_pin_active) //если пин изменился то снимаем выделение с предыдущего
            g_function_active->setPinSelect(g_pin_active,false);

        g_pin_active = g_pin_active_temp;  //запоминаем пин на который наведен курсор
        if (g_function_active_temp) //если курсор в районе функции то выделяем пин
            g_function_active_temp->setPinSelect(g_pin_active,true);
    }
    else{
        if (g_function_active && g_pin_active){ //если не в районе
            g_function_active->setPinSelect(g_pin_active,false);
            g_pin_active = 0;
        }

    }

    if (g_function_active_temp){
        g_function_active = g_function_active_temp;
        g_function_active->setSelect(true); //выделяем функцию
    }
    else{
        if (g_function_active){
            g_function_active->setSelect(false); //снимаем выделение функции
            g_function_active = 0;
        }
    }


}
