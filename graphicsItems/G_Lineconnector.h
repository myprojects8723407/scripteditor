#ifndef G_LINECONNECTOR_H
#define G_LINECONNECTOR_H

#include <QObject>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QPainterPath>
#include <QPainter>
#include <QCursor>
#include <QDebug>
#include <QGraphicsSceneMouseEvent>


class G_Pin;

class G_LineConnector: public QObject,public QGraphicsItem
{
    Q_OBJECT
public:
   explicit G_LineConnector(QObject *parent=0);
   QRectF boundingRect() const;
   void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
              QWidget *widget);
   void setColor(QColor);
   void set_start_pos(QPointF);
   void set_end_pos(QPointF);
   void set_pos(QPointF point_start,QPointF point_end);
   void setSelected(bool);
   void addConnector(G_Pin* pin){pin_connected.append(pin);}
    QList <G_Pin*> getConnectors(){return pin_connected;}
    void removePin(G_Pin* pin);

    QPointF start;
    QPointF end;

public slots:


private:

  bool is_selected = false;

  QColor color;
  double angle;
  QString text;
  int id_parent; //id сценария которому принадлежит функция
  int id; //id из базы данных


  QString name,comment;
  QList <G_Pin*> pin_connected;



};

#endif // G_LINECONNECTOR_H
