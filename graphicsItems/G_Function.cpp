#include "G_Function.h"
//#include "g_pin.h"







G_Function::G_Function(int id_,QObject *parent) : QObject(parent), QGraphicsItem()
{
    ramka = 6;
    otstup_stroka = 1.5;
    height = 0;
    width = 0;
    height_head = 0;
    razdelitel = 20;
    mouse_left_button_press = 0;
    razmer_connector = 4;
    otstup_connector = 2;
    prozrachnost = 200;
    id_parent = -1;

    font.setPointSizeF(12);
    QFontMetrics fm(font);
    height_font=fm.height();
    is_selected = false;
    is_select = false;



    // id_script=id_script_; //присваиваем id сценария в котором данная функция
    id=id_; // id в базе данных
}

G_Function::~G_Function()
{
    /*foreach (G_Pin* pin, pins) {
       delete pin;
       pin = 0;
    }

    pins.clear();*/
}

QRectF G_Function::boundingRect() const
{
    return QRectF(-width/2.0-ramka-1,0-ramka-1,width+ramka*2+4,height+ramka*2+4);
}

void G_Function::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                       QWidget *widget)
{

    //painter->rotate(angle);


    if (is_select)
        painter->setPen(QPen(Qt::yellow,1));
    if (is_selected)
        painter->setPen(QPen(QColor(255,255,255,120),4));
    else  painter->setPen(QPen(color,4));

    if (is_error)
        painter->setPen(QPen(Qt::red,4));


    QPainterPath path_all_rect;
    path_all_rect.addRoundedRect(-width/2.0-ramka,0-ramka,width+ramka*2,height+ramka*2, 5, 5);
    painter->fillPath(path_all_rect, QColor(0,0,0,prozrachnost));
    painter->drawPath(path_all_rect);

    QPainterPath path_head;
    path_head.addRoundedRect(QRectF(head_rect), 5, 5);
    painter->fillPath(path_head, head_color);
    painter->drawPath(path_head);

    painter->setFont(font);
    painter->setPen(Qt::white);
    painter->drawText(-width/2.0,0,width,height_font,0,function_name);


    foreach (G_Pin* pin, pins) {

        if (pin->is_selected){
            painter->setPen(Qt::yellow);
            painter->drawRect(pin->rect_selected);
        }
        painter->setPen(Qt::white);
        painter->drawText(pin->rect_text,0,pin->text);

        painter->setPen(pin->color);
        if (pin->is_connected) //если контакт соединен то закрашиваем
            painter->fillPath(pin->path, pin->color);


        painter->drawPath(pin->path);
    }

}


void G_Function::setError(QString err)
{
    error=err;
}

void G_Function::setColor(QColor c)
{
    color = c;
}

void G_Function::setText(QString text_)
{
    text=text_;
}

void G_Function::setId(int id_)
{
    id=id_;
}

void G_Function::setService(Service service_)
{
    service = service_;
}

void G_Function::setFunction(Function function_)
{
    function = function_;
    function_name = function.comand_name;
    height = height_font; //высота заголовка
    head_color = function_.color_head;


    QFontMetrics fm(font);
    width=fm.width(function.comand_name);
    head_rect = QRect(-width/2.0-ramka,0-ramka,width+ramka*2,height_font+ramka*2);



}

void G_Function::setParameters(QList<Parameter> parameters_)
{
    parameters=parameters_;
    int old_height = 0;

    int max_width_IN=0;
    int max_width_OUT=0;
    QFontMetrics fm(font);
    //определяем максимальную ширину прямоугольника
    foreach (Parameter parameter, parameters) {
        int width_temp=fm.width(parameter.parameters_name);
        if (parameter.inout_type == 0){
            if (max_width_IN<=width_temp) max_width_IN=width_temp;
        }
        else
        {
            if (max_width_OUT<=width_temp) max_width_OUT=width_temp;
        }
    }

    //если нужно увеличение ширины прямоугольника
    if (max_width_IN + max_width_OUT + razdelitel+otstup_connector*2>width){
        width = max_width_IN + max_width_OUT + razdelitel+otstup_connector*2;
        head_rect = QRect(-width/2.0-ramka,0-ramka,width+ramka*2,height_font+ramka*2);
    }

    int height_IN = height_font*1.5/*+height_font*/;
    int height_OUT = height_font*1.5/*+height_font*/;

    //для объединения параметров типа MAP
    QMap <QString,QList <Parameter> > map;
    foreach (Parameter parameter, parameters) {
        map[parameter.parameters_name].append(parameter);
    }


    for (int i=0; i<2; i++){

        //находим координаты прямоугольников для переменных

        foreach (QString parameters_name, map.keys()) {
            //вход и выход отображаем первыми затем остальные параметры
            if ((map.value(parameters_name).value(0).parameters_type.contains("connector") && i==0) ||
                    (!map.value(parameters_name).value(0).parameters_type.contains("connector") && i==1)){ //сверху рисуем коннекторы ниже параметры

                G_Pin *pin=new G_Pin(this);
                pin->setPinName(parameters_name); //порядковый номер пина
                pin->parameter = map.value(parameters_name).value(0);

                if (!map.value(parameters_name).value(0).parameter_type.contains("default")) //не отображаем названия коннекторов по умолчанию только треугольники
                        pin->text = parameters_name;

                pin->type = map.value(parameters_name).value(0).inout_type;
                int width_temp=fm.width(parameters_name);

                if (pin->parameter.inout_type == 0){
                    pin->rect_text = QRect(-width/2.0+otstup_connector+razmer_connector,height_IN,width_temp,height_font);
                    height_IN = height_IN + height_font;
                    pin->point_connector=QPoint(head_rect.left()+otstup_connector+razmer_connector,pin->rect_text.center().y());
                    pin->rect_selected = pin->rect_text;
                    pin->rect_selected.setLeft(head_rect.left());
                }
                else
                {
                    pin->rect_text = QRect(width/2.0 - width_temp-otstup_connector-razmer_connector,height_OUT,width_temp,height_font);
                    height_OUT = height_OUT + height_font;
                    pin->point_connector=QPoint(head_rect.right()-razmer_connector-otstup_connector,pin->rect_text.center().y());
                    pin->rect_selected = pin->rect_text;
                    pin->rect_selected.setRight(head_rect.right());

                }

                //рисуем треугольник
                if (pin->parameter.parameters_type.contains("connector")){
                    pin->path.moveTo (pin->point_connector.x()+razmer_connector,pin->point_connector.y());
                    pin->path.lineTo (pin->point_connector.x()-razmer_connector, pin->point_connector.y()+razmer_connector);
                    pin->path.lineTo (pin->point_connector.x()-razmer_connector, pin->point_connector.y()-razmer_connector);
                    pin->path.lineTo (pin->point_connector.x()+razmer_connector,pin->point_connector.y());

                }
                else{
                    pin->path.addEllipse(pin->point_connector,razmer_connector,razmer_connector);
                }


                pin->is_selected = false;
                pin->is_connect = false;
                pin->is_connector = false;
                pin->is_connected = false;

                int prozrachnost = 200;
                if (pin->parameter.parameters_type.contains("connector"))
                    pin->color.setRgb(255,255,255,prozrachnost);
                else if (pin->parameter.parameters_type=="number")
                    pin->color.setRgb(255,255,0,prozrachnost);
                else if (pin->parameter.parameters_type=="string")
                    pin->color.setRgb(255,0,0,prozrachnost);
                else pin->color.setRgb(0,0,255,prozrachnost);


                pins.append(pin);
            }
        }
    }

    if (height_IN>height_OUT)
        height =  height_IN;
    else
        height =  height_OUT;


    // rect = QRect(-width/2.0-ramka,0-ramka,width+ramka*2,height+ramka*2);

}

void G_Function::setSelect(bool flag)
{
    if (is_select!=flag)
        this->update();
    is_select = flag;
}

bool G_Function::getSelect()
{
    return this->is_select;
}

void G_Function::setPinSelect(G_Pin* pin, bool flag)
{
    if (pin->is_selected!=flag)
        this->update();
    pin->is_selected = flag;
}

bool G_Function::getPinSelect(G_Pin* pin)
{
    pin->is_selected;
}

G_Pin* G_Function::getPinsByNum(QString name)
{
    foreach (G_Pin* pin, pins) {
        if (pin->getPinName() == name){
            return pin;
        }
    }
    return 0;
}

QList <G_Pin*> G_Function::getPins()
{
    return pins;
}

QRectF G_Function::getRectHead()
{
    return head_rect;
}


void G_Function::setAngle(double angle_)
{
    angle = angle;

}

void G_Function::slot_set_state(int state)
{
    if (state==0){
        color = Qt::green;
    }

}







