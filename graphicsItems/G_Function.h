#ifndef G_FUNCTION_H
#define G_FUNCTION_H

#include <QObject>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QPainterPath>
#include <QPainter>
#include <QCursor>
#include <QGraphicsSceneMouseEvent>

#include "G_Lineconnector.h"
#include "DataBase.h"
#include "G_Pin.h"




class G_Function : public QObject,public QGraphicsItem
{
    Q_OBJECT

public:
    explicit G_Function(int id,QObject *parent=0);
     ~G_Function();

    void setColor(QColor);
    void setText(QString text_);

    void setAngle(double);
    void setError(QString err);

    void setId(int id);
    void setService(Service service);
    void setFunction(Function function);
    Function getFunction(){return function;}
    void setParameters(QList <Parameter> parameters);

    void set_isSelected(bool flag){is_selected = flag;}
    bool get_isSelected(){return is_selected;}

    void setSelect(bool);
    bool getSelect();
    void setPinSelect(G_Pin*,bool);
    bool getPinSelect(G_Pin*);
    G_Pin* getPinsByNum(QString name);

    void setError(bool flag){is_error = flag;}


    G_Pin* getPinConnectorIncoming(){return pin_connector_in;}
    G_Pin* getPinConnectorOutgoing(){return pin_connector_out;}

    QList<G_Pin*> getPinsAll(){return pins;}

    int getId(){
        return id;
    }

    void setParent(int id){id_parent = id;}
    int getParent(){return id_parent;}


    QList <G_Pin*> getPins();
    QRectF getRectHead();


    QColor color;
    double angle;

    QString text;
    QString function_name;
    QString par_in;
    QString par_out;
    int id_script; //id сценария которому принадлежит функция


    int type;
    QString error;
    QString name,comment;

    QList <G_LineConnector*> line_connector;

    bool mouse_left_button_press;
    QPoint delta;


signals:
    void sig_element_set_pos(int id,QPoint point);  //изменяем позицию элемента в базе
    void sig_function_selected(int id);//двойное нажатие на элемент
    void sig_element_move(int id,QPoint point);  //перетаскиваем элемент
    void sig_selected(int);// нажали мышкой на элемент сценария

public slots:
    void slot_set_state(int);


private:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;

    Service service;
    Function function;
    QList<Parameter> parameters;
    QPoint point;  //позиция в координатах сцены
    int width; //ширина прямоугольника
    int height_head; //высота прямоугольника заголовка окна
    int height; //высота всего прямоугольника

    int ramka;
    float otstup_stroka;
    int height_font;
    int razdelitel; //расстояние разделителя входящих и выходящих переменных
    bool is_select = false; //в прямоугольник наведена мышь
    bool is_selected = false; //выбран
    bool is_error = false; //ошибка выполнения для выделения красным цветом
    int razmer_connector;
    int otstup_connector; //отступ от края рамки
    int prozrachnost; //прозрачность прямоугольников

    QPainterPath path_all_rect;
    QFont font;

    int id_parent; //id сценария которому принадлежит функция


    int id; //id из базы данных


    QRect head_rect;
    QRect head_rect_text;
    QRect rect_connector_in;
    QRect rect_connector_out;

    QList <G_Pin*> pins;

    G_Pin* pin_connector_out = 0;
    G_Pin* pin_connector_in = 0;


    QColor head_color;



    int deltaY = 0;

};

#endif // G_FUNCTION_H
