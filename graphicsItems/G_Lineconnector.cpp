#include "G_Lineconnector.h"
#include "G_Pin.h"
#include "qmath.h"

G_LineConnector::G_LineConnector(QObject *parent): QObject(parent), QGraphicsItem()
{

}

QRectF G_LineConnector::boundingRect() const
{
    int startX = start.x();
    int startY = start.y();
    int endX = end.x();
    int endY = end.y();

    return QRectF(startX,startY,endX,endY);
}

void G_LineConnector::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                           QWidget *widget)
{
    if (is_selected)
        painter->setPen(QPen(color,4));
    else
        painter->setPen(QPen(color,2));



    int width=fabs(end.x()-start.x());
    if (width<100) width = 100;

    QPointF point0(start.x() , start.y());

    QPointF point1(start.x()+width/2.0,start.y());
    QPointF point2(end.x()-width/2.0,end.y());
    QPointF point3(end.x() , end.y());

    QPainterPath cubPath;
    cubPath.moveTo(point0);
    cubPath.cubicTo (point1,point2, point3); // Входящая точка 1 (170,50), точка 2 (90,200), конечная точка (width (), heigt ())

    painter->drawPath(cubPath);
}


void G_LineConnector::setColor(QColor c)
{
    color = c;
}


void G_LineConnector::set_pos(QPointF point_start,QPointF point_end)
{
    start = point_start;
    end = point_end;
    //this->update();
}

void G_LineConnector::setSelected(bool flag)
{
    is_selected = flag;
}

void G_LineConnector::removePin(G_Pin *pin)
{

          pin_connected.removeOne(pin);
     int gg=0;
}

void G_LineConnector::set_start_pos(QPointF point)
{
    start = point;
    //this->update();
}

void G_LineConnector::set_end_pos(QPointF point)
{
    end = point;
    //this->update();
}
