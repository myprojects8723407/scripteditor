#ifndef LISTWIDGETPARAMRETERS_H
#define LISTWIDGETPARAMRETERS_H

#include <QWidget>
#include <QTableWidget>
#include <QHeaderView>
#include <QLineEdit>
#include <QComboBox>
#include <QDoubleValidator>
#include "Structures.h"


class ListWidgetParamreters : public QTableWidget
{
    Q_OBJECT
public:
    explicit ListWidgetParamreters(QWidget *parent = 0);


signals:
     void  sig_changeParameterValue(ScriptElement * element,int); //изменили значение по умолчанию

public slots:
    void slot_set_parameters_to_listWidget(ScriptElement *);
    void slot_boxChanged(QString);
    void slot_editingFinished();

private:
    QMap <QString,QList <Parameter> > map_parameters_in; //сгрупперованные параметры по имени
    QMap <QString,QList <Parameter> > map_parameters_out; //сгрупперованные параметры по имени
    ScriptElement* element;
    QList <QLineEdit*> lineEdites;
    QList <QComboBox*> comboBoxes;
    void parametersChanged(QString name, QString value);
    void clearData();
    void insertParameters(QMap<QString, QList<Parameter> > parameters);
    void addLineText(QString text);
    void insertParameterIn(QMap<QString, QList<Parameter> > parameters_map);
};

#endif // LISTWIDGETPARAMRETERS_H
