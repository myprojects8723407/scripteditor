#ifndef STRUCTURES_H
#define STRUCTURES_H


#include <QObject>
#include <QMap>
#include <QPoint>
#include <QColor>

struct Server
{
    int      server_id;
    QString  server_name;
    int      server_number;
    QString  server_ip;
    bool     server_is_deleted;
};

struct Service
{
    int      service_id;
    int      server_index;
    QString  service_name;
    int      service_identation;
    QString  service_system_name;
    bool     service_is_deleted;

    QString server_ip; //ip сервера на котором расположена служба
};

struct Function{
    int         id;
    int         service_index;
    QString     comand_name;
    int         comand_function_id;
    int         comand_is_deleted;
    QString     comand_about;
    QString     script;

    QColor color_head; //цвет заголовка
};

struct Parameter{
    int         id;
    int         comand_index;
    int         parameter_number;
    QString     parameters_name;
    QString     parameter_name;
    QString     parameter_value;
    QString     parameters_type;
    QString     parameter_type;
    int         inout_type;

    QString     value; //результат вычислений либо заданное значение
    int id_element_connect; //id элемента сценария с которым соединен пин
    QString parameters_name_connect;  //имя параметра с которым соединен пин
    bool is_delete; //отмечен для удаления
    int getOrSetValue; //параметр отночится к получению или заданию глобальной переменной
};


struct ScriptElement{
    int id;
   // int id_parent;
    int id_script;
    Service service; //описание службы которой принадлежит элемент скрипта
    Function function; //функция
    QList <Parameter> parameters; //параметры функции
    QList<int> id_elements_child; //id элементов сценари далее по цепочке выполнения сценария, к каждому создается отдельный выход из функции вход всегда один
    QPoint point; //координаты элемента
    QString comment;

    bool is_new; //новый элемент
    bool is_update; //элемент уже есть в базе обновить данные
    bool is_deleted; //удалить элемент
    QString script_error; //ошибка выполнения


};

struct Script{


    int id;
    QString name;
    QString comment;
    QMap<int,ScriptElement*> elemens;

    QPoint point; //последние координаты редактирования
    bool is_new; //новый элемент
    bool is_start; //запущен на выполнение или завершен


};

enum StateFunction
{
    Process,      // 0 - функция запущена на выполнение
    EndOk,     // 1 - выполнена успешно
    EndError,        // 2 - выполнена с ошибкой
    Finish, //3-завершен полностью
};
#endif // STRUCTURES_H
