#ifndef WIDGWTSCREATOR_H
#define WIDGWTSCREATOR_H

#include <QObject>
#include <QDir>
#include <QToolBar>
#include <QTimer>
#include "Scenariy_Worker.h"
#include "DataBase.h"
#include "Scenariy_Worker.h"
#include "forms/FormScriptsRedactor.h"
#include "ScriptWorkerManager.h"


class WidgwtsCreator : public QObject
{
    Q_OBJECT
public:
    explicit WidgwtsCreator(QObject *parent = 0);

    DataBase *bd;
    QToolBar *toolbar;
    ScriptWorker *sqriptWorker; //поток выполняющий скрипт
    FormScriptRedactor *formScriptsRedactor; //форма для работы с выбором скрипта
    QTimer *timer_plan_control;
    QTimer *timer_script_start;
    ScriptWorkerManager *scriptWorkerManager;

signals:
    void sig_reconnect_BD(QString type, QString ip,QString name,int port,QString username, QString password);
    void sig_remove_seans();
    void sig_start_script(); //запуск текущего загруженного сценария

public slots:

    //void slot_result_script(bool);
    //void slot_script_start();
};

#endif // WIDGWTSCREATOR_H
