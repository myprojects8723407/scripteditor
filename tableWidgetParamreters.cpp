#include "tableWidgetParamreters.h"

ListWidgetParamreters::ListWidgetParamreters(QWidget *parent) : QTableWidget(parent)
{
    this->setColumnCount(3);
    QStringList horzHeaders;
    horzHeaders << "Имя" << "Тип"<<"Значение" ;
    this->setHorizontalHeaderLabels( horzHeaders );
    this->verticalHeader()->hide();
    connect(this,SIGNAL(cellEntered(int,int)),this,SLOT(slot_cellChanged(int,int)),Qt::QueuedConnection);
    //this->horizontalHeader()->setVisible(false);
}

//--------------------------------вставка параметризованного входного параметра  ------------------------
void ListWidgetParamreters::insertParameterIn(QMap <QString,QList <Parameter> > parameters_map)
{

}
//--------------вставка параметров в ячейки ----------------------------------
void ListWidgetParamreters::insertParameters(QMap <QString,QList <Parameter> > parameters_map)
{
    foreach (QString parameters_name, parameters_map.keys()) {

        Parameter parameter = parameters_map.value(parameters_name).value(0);

        if (parameter.parameters_type.contains("connector"))  //не добавляем в таблицу переменных
            continue;

        this->insertRow(this->rowCount());




        QTableWidgetItem* taskItem = new QTableWidgetItem();
        taskItem->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
        taskItem->setText(parameters_name);
        this->setItem(this->rowCount()-1,0,taskItem);


        taskItem = new QTableWidgetItem();
        taskItem->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
        taskItem->setText(parameters_map.value(parameters_name).value(0).parameters_type); //все параметры одного типа
        this->setItem(this->rowCount()-1,1,taskItem);

        taskItem = new QTableWidgetItem();
        taskItem->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);



        if (parameter.inout_type == 0)
        { //входной параметр с реакцией на редактирование

            if (parameter.parameters_type.contains("map")){
                QComboBox* box = new QComboBox;
                comboBoxes.append(box);
                box->setObjectName(parameters_name);
                foreach (Parameter parameter, parameters_map.value(parameters_name)) {
                    box->addItem(parameter.parameter_name,parameter.parameter_value);
                }

                this->setCellWidget(this->rowCount()-1,2,box);
                connect( box, SIGNAL(editTextChanged(QString)) , SLOT(slot_boxChanged(QString) ));
            }
            else{

                QLineEdit* lineEdit = new QLineEdit;
                lineEdit->setObjectName(parameters_name);
                if (parameter.parameters_type.contains("byte")){
                    lineEdit->setValidator(new QIntValidator(0,255));
                }
                else if (parameter.parameter_type.contains("word")){
                    lineEdit->setValidator(new QIntValidator(0,65535));
                }
                else if (parameter.parameter_type.contains("dword")){
                    lineEdit->setValidator(new QIntValidator(0,4294967295));
                }
                else if (parameter.parameter_type.contains("double")){
                    lineEdit->setValidator(new QDoubleValidator());
                }
                else if (parameter.parameter_type.contains("datetime")){
                    lineEdit->setValidator(new  QRegExpValidator(QRegExp("([1-9]|[12][0-9]|3[01]{1,2})/([1-9]|[12]{1,2})/(19[0-9][0-9]|20[0-9][0-9])")));
                }
                else if (parameter.parameter_type.contains("vector")){
                    this->setItem(this->rowCount()-1,2,taskItem);
                    continue;
                }

                lineEdit->setText(parameter.value);
                this->setCellWidget(this->rowCount()-1,2,lineEdit);

                connect( lineEdit, SIGNAL(returnPressed()) , SLOT(slot_editingFinished() ));
                lineEdites.append(lineEdit);
            }
        }
        if (parameter.inout_type == 1) //выходной просто для показа результата
        {
             taskItem->setText(parameter.value);
             this->setItem(this->rowCount()-1,2,taskItem);
        }

    }
}

void ListWidgetParamreters::addLineText(QString text)
{
    this->insertRow(this->rowCount());
    this->setSpan(this->rowCount()-1,0,1,3);
    QFont font;
    font.setPointSizeF(12);
    QTableWidgetItem *item = new QTableWidgetItem;
    item->setFont(font);
    this->setItem(this->rowCount()-1,0,item);
    item->setText(text);
    item->setTextAlignment(Qt::AlignJustify); // 1
    item->setTextAlignment(Qt::AlignHCenter); // 2
    item->setTextAlignment(Qt::AlignVCenter); // 2

}

void ListWidgetParamreters::clearData()
{
    foreach (QLineEdit* lineEdit, lineEdites) {
        lineEdit->disconnect(SIGNAL(editingFinished()));
        delete lineEdit;
    }
    foreach (QComboBox* box, comboBoxes) {
        box->disconnect(SIGNAL(editingFinished()));
        delete box;
    }

    map_parameters_in.clear();
    map_parameters_out.clear();
    lineEdites.clear();
    comboBoxes.clear();
    this->clear();
}

void ListWidgetParamreters::slot_set_parameters_to_listWidget(ScriptElement * element_)
{
    element = element_;
    clearData();
    this->setRowCount(0);

    //сортировка параметров
    foreach (Parameter parameter, element->parameters) {
        if (parameter.inout_type == 0)
            map_parameters_in[parameter.parameters_name].append(parameter);
        if (parameter.inout_type == 1)
            map_parameters_out[parameter.parameters_name].append(parameter);
    }

    if (map_parameters_in.count()>0){
        addLineText("Входные параметры");
        insertParameters(map_parameters_in);
    }

    this->insertRow(this->rowCount());

    if (map_parameters_out.count()>0){
        addLineText("Выходные параметры");
        insertParameters(map_parameters_out);
    }


}

void ListWidgetParamreters::slot_editingFinished()
{
    if( QLineEdit* edit = qobject_cast< QLineEdit* >( sender() ) )
    {
        parametersChanged(edit->objectName(), edit->text());
    }
}

void ListWidgetParamreters::parametersChanged(QString name, QString value)
{
    int num=0;
    foreach (Parameter parameter, element->parameters) {
        if (parameter.parameters_name == name){
            element->parameters[num].value = value;
            element->is_update = true;
            //emit sig_changeParameterValue(element, num);
            break;
        }
        num++;
    }

}

void ListWidgetParamreters::slot_boxChanged(QString)
{
    if( QComboBox* box = qobject_cast< QComboBox* >( sender() ) )
    {
        parametersChanged(box->objectName(), box->currentText());
    }
}
