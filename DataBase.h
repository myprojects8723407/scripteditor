#ifndef BD_H
#define BD_H

#include <QObject>
#include <QSqlRecord>
#include <QCoreApplication>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlQueryModel>
#include <QtSql/QSqlError>
#include <QMessageBox>
#include <QTimer>
#include <QFile>
#include <QTextStream>
#include <QSqlTableModel>
#include <QTableView>
#include "Structures.h"

class GlobalValues;



class DataBase : public QObject
{
    Q_OBJECT

    static Function get_function_byQuery(QSqlQuery query);
    static bool get_functions_byQuery(QSqlQuery query, QList<Function> &functions);
    static Service get_service_byQuery(QSqlQuery query);
    static bool get_service_byQuery(QSqlQuery query, QList<Service> &services);
    static Parameter get_parameter_byQuery(QSqlQuery query);
    static bool get_parameters_byQuery(QSqlQuery query, QList<Parameter> &parameters,bool append_outers = true);
    void set_parameters_value_byElementId(ScriptElement *el);

    bool setSqliteCachePath(QString dbPath);
    void CreateTables();

    QString bd_type;

    QSqlDatabase db;
    QString BD_NAME;
    QString IP_BD;
    int Port_BD;
    QString UserName;
    QString Password;
    QTimer *timer_status;




    QString erororConnect();
    bool PostgresConnect();
    bool bdConnect(QString type, QString ip, QString name, int port, QString username, QString password);

    void get_max_IdElements();
    void delete_element(ScriptElement *el);
    void save_global_values();
    static void getServerFromQuery(QSqlQuery query, Server server);

public:
    explicit DataBase(QObject *parent = nullptr);
    void read_services();

    static int service_index_function_c; //индекс сервиса с функциями c++

    static bool get_services(QList <Service> &);
    static bool get_service_byId(int id,Service&);
    static bool get_service_byIdentation(int service_identation,Service&);
    static bool service_bySystemName(QString service_system_name,Service&);
    static bool service_byServiceName(QString service_name,Service&);

    static bool get_functions(QList <Function>&);
    static bool get_functions_byServiceId(int service_id,QList <Function>&);
    static bool get_function_byFunctionId(int service_identation, int comand_function_id, Function&);

    static bool get_parameters_byServiceFunctionIdentation(int command_function_id, QList<Parameter> &parameters,bool append_outers = true);


    static QMap<QString, Script* > read_scripts();

    static QMap <QString, Script* > Scripts;

    static void AppendOuterParameter(QList<Parameter>& parameters, int type);


    static bool get_function_byName(int service_identation, QString comand_name, Function &function);

    static bool get_serverById(int server_id, Server &server); //получить параметры сервера по индексу
    static bool get_serverByNumber(int server_number, Server &server); //получить параметры сервера по индексу
    static void appendScript(Script*);

    static int getScriptIdByName(Script *script);
    static void removeScript(QString);
    static void removeService(int);
    static void removeFunction(int);
    static void removeParameter(int);

    static void removeGlobalParameter(QString name);
signals:
    void sig_error(bool); //ошибки БД
    void sig_bd_open(bool); //результат открытия бд
    void sig_scripts_read_from_bd(QMap <QString, Script* > script);
    void sig_set_id_last(int id_last);
    void sig_updateWidgetFunctions(); //обновить список функций
    void sig_read_global_values();
    void sig_bd_scripts_loaded();

public slots:
    void slot_element_add(ScriptElement *el);
    //void slot_script_selected(int,QString,QString);
    void slot_element_change(ScriptElement *el);
    void slot_element_set_pos(int id_element,QPoint point);
    void save_function_to_bd(int,QString);
    void slot_changeParameterValue(ScriptElement*, int num);
    void slot_save_function_to_bd(ScriptElement*);

    void slot_reconnect(QString type, QString ip, QString name, int port, QString username, QString password);
    void slot_load_scripts();
    void slot_save();
};

#endif // BD_H
