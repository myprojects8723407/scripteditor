#include "DataBase.h"
#include "GlobalValues.h"

QMap <QString, Script* > DataBase::Scripts;
int DataBase::service_index_function_c = 20000;

DataBase::DataBase(QObject *parent) : QObject(parent)
{

}

//------------ Получаем последний id элемента в таблице БД -----------------
void DataBase::get_max_IdElements()
{

    QSqlQuery query(QSqlDatabase::database("db"));
    bool rez=query.exec("SELECT id_element FROM script_element WHERE id_element = (SELECT MAX(id_element) FROM  script_element);");
    if (rez)
    {
        while(query.next())
        {
            int id_last = query.value(0).toInt();
            if (id_last>-1){
                emit sig_set_id_last(id_last);
            }
            else emit sig_set_id_last(0);
        }
    }
    else  emit sig_set_id_last(0);

}

//------------ Создаие таблиц БД -----------------
void DataBase::CreateTables()
{
    if (bd_type=="QPSQL" || bd_type=="QSQLITE"){

        QString key;
        if (bd_type == "QPSQL") key = "SERIAL PRIMARY KEY";
        if (bd_type == "QSQLITE") key = "INTEGER PRIMARY KEY AUTOINCREMENT";
        QSqlQuery query(QSqlDatabase::database("db"));
        query.exec("CREATE TABLE services (service_id "+key+",server_index bigint NOT NULL,service_name text,service_identation integer,service_system_name text,service_is_deleted  boolean NOT NULL DEFAULT false  );");
        query.exec("CREATE TABLE functions (id "+key+",service_index integer NOT NULL,command_duration integer,command_name text,command_function_id integer NOT NULL,command_is_deleted integer,command_about text, script text);");
        query.exec("CREATE TABLE parameters (id "+key+",command_index integer NOT NULL,parameter_number integer NOT NULL,parameters_name text,parameter_name text,parameter_value text,parameters_type text,parameter_type text, inout_type integer);");

        query.exec("CREATE TABLE scripts (id "+key+",name text NOT NULL,comment text);");
        query.exec("CREATE TABLE plans_work (id "+key+",id_ka integer NOT NULL,start timestamp NOT NULL,stop timestamp NOT NULL,modes text NOT NULL,script text NOT NULL, result text, cu text, comment_result text);");
        query.exec("CREATE TABLE results (id "+key+",name text,comment text);");
        query.exec("CREATE TABLE cu (id "+key+",date_start timestamp NOT NULL,date_end timestamp NOT NULL,name text NOT NULL,cu Bytea);");
        query.exec("CREATE TABLE script_element (id "+key+", id_element integer NOT NULL,"
                                                          "id_script integer NOT NULL, x integer, y integer, id_service integer,"
                                                          "id_command integer, script text, comment text);");
        query.exec("CREATE TABLE script_parameters (id "+key+", id_element integer NOT NULL, parameter_name text,"
                                                             "value text, id_element_connect integer, parameters_name_connect text);");
        query.exec("CREATE TABLE value_types (id "+key+", type text NOT NULL, minimum_value integer, maximum_value integer);");
        query.exec("CREATE TABLE parameter_types (id "+key+", type text NOT NULL);");
        query.exec("CREATE TABLE global_parameters (id "+key+", name text NOT NULL, type text, value text);");

        query.exec( "SELECT service_id FROM services WHERE service_name = 'Управление выполнением сценария'");
        int index_services_drive = -1;
        bool services_drive_create = false;
        if (!query.next())
        {
            query.exec("INSERT INTO services (server_index,service_name,service_identation,service_system_name,service_is_deleted) "
                       "VALUES(0,'Управление выполнением сценария',"+QString::number(service_index_function_c)+",'QtScript',0);");
            index_services_drive = query.lastInsertId().toInt();

            //удаляем для записи заново чтобы связать id вновь созданного сервиса
            query.exec("DELETE FROM functions WHERE command_name='Получить' ");
            query.exec("DELETE FROM functions WHERE command_name='Установить' ");
            query.exec("DELETE FROM functions WHERE command_name='Пауза' ");
            query.exec("DELETE FROM functions WHERE command_name='Скрипт' ");
            query.exec("DELETE FROM functions WHERE command_name='Старт' ");
            services_drive_create = true;

        }
        else  index_services_drive = query.value(0).toInt();


        query.exec( "SELECT id FROM functions WHERE command_name = 'Получить'");
        if (!query.next())
        {
            query.exec("INSERT INTO functions (service_index,command_duration,command_function_id,command_about,command_name,script) "
                       "VALUES("+ QString::number(index_services_drive)+", 0 , 0 , "+
                       "'Получает значение глобальной переменной',"+
                       "'Получить','');");
            int index_get_value = query.lastInsertId().toInt();

            query.exec("INSERT INTO  parameters  (command_index , parameter_number , parameters_name , parameter_name , parameter_value, parameters_type,parameter_type ,inout_type)"
                       "VALUES ("+QString::number(index_get_value)+",0,'Параметр','','','','',1)");
        }


        query.exec( "SELECT id FROM functions WHERE command_name = 'Установить'");
        if (!query.next())
        {
            query.exec("INSERT INTO functions (service_index,command_duration,command_function_id,command_about,command_name,script) "
                       "VALUES("+ QString::number(index_services_drive)+", 0 , 0 , "+
                       "'Устанавливает значение глобальной переменной',"+
                       "'Установить','');");
            int index_set_value = query.lastInsertId().toInt();

            query.exec("INSERT INTO  parameters  (command_index , parameter_number , parameters_name , parameter_name , parameter_value, parameters_type,parameter_type ,inout_type)"
                       "VALUES ("+QString::number(index_set_value)+",0,'Параметр','','','','',1)");
        }

        query.exec( "SELECT id FROM functions WHERE command_name = 'Пауза'");
        if (!query.next())
        {
            query.exec("INSERT INTO functions (service_index,command_duration,command_function_id,command_about,command_name,script) "
                       "VALUES("+ QString::number(index_services_drive)+", 0 , 0 , "+
                       "'Пауза в выполнении сценария. На входе количество миллисекунд.',"+
                       "'Пауза','');");
            int last_index = query.lastInsertId().toInt();

            query.exec("INSERT INTO  parameters  (command_index , parameter_number , parameters_name , parameter_name , parameter_value, parameters_type,parameter_type ,inout_type)"
                       "VALUES ("+QString::number(last_index)+",0,'Длит.','','','Number','',0)");

        }

        query.exec( "SELECT id FROM functions WHERE command_name = 'Скрипт'");
        if (!query.next())
        {
            query.exec("INSERT INTO functions (service_index,command_duration,command_function_id,command_about,command_name,script) "
                       "VALUES("+ QString::number(index_services_drive)+", 0 , 0 , "+
                       "'Выполняет другой скрипт заданный именем и номером стартового элемента.',"+
                       "'Скрипт','');");
            int last_index = query.lastInsertId().toInt();

            query.exec("INSERT INTO  parameters  (command_index , parameter_number , parameters_name , parameter_name , parameter_value, parameters_type,parameter_type ,inout_type)"
                       "VALUES ("+QString::number(last_index)+",0,'Имя','','','String','',0)");
            query.exec("INSERT INTO  parameters  (command_index , parameter_number , parameters_name , parameter_name , parameter_value, parameters_type,parameter_type ,inout_type)"
                       "VALUES ("+QString::number(last_index)+",0,'Номер','','','Number','',0)");


        }

        query.exec( "SELECT id FROM functions WHERE command_name = 'Старт'");
        if (!query.next())
        {
            query.exec("INSERT INTO functions (service_index,command_duration,command_function_id,command_about,command_name,script) "
                       "VALUES("+ QString::number(index_services_drive)+", 0 , 0 , "+
                       "'Стартовый элемент скрипта.',"+
                       "'Старт','');");

            int last_index = query.lastInsertId().toInt();

            query.exec("INSERT INTO  parameters  (command_index , parameter_number , parameters_name , parameter_name , parameter_value, parameters_type,parameter_type ,inout_type)"
                       "VALUES ("+QString::number(last_index)+",0,'Выход','','','connector_out','default',1)");
        }

        query.exec( "SELECT id FROM functions WHERE command_name = 'Условие'");
        if (!query.next())
        {
            query.exec("INSERT INTO functions (service_index,command_duration,command_function_id,command_about,command_name,script) "
                       "VALUES("+ QString::number(index_services_drive)+", 0 , 0 , "+
                       "'Простое ветвление.',"+
                       "'Условие','if (a0>1) out[0]=\"Истина\"; else out[0]=\"Ложь\";');");

            int last_index = query.lastInsertId().toInt();

            query.exec("INSERT INTO  parameters  (command_index , parameter_number , parameters_name , parameter_name , parameter_value, parameters_type,parameter_type ,inout_type)"
                       "VALUES ("+QString::number(last_index)+",0,'Число','','','Number','',0)");
            query.exec("INSERT INTO  parameters  (command_index , parameter_number , parameters_name , parameter_name , parameter_value, parameters_type,parameter_type ,inout_type)"
                       "VALUES ("+QString::number(last_index)+",0,'Истина','','','connector_out','',1)");
            query.exec("INSERT INTO  parameters  (command_index , parameter_number , parameters_name , parameter_name , parameter_value, parameters_type,parameter_type ,inout_type)"
                       "VALUES ("+QString::number(last_index)+",0,'Ложь','','','connector_out','',1)");
        }



        query.exec( "SELECT service_id FROM services WHERE service_name = 'Математические'");
        int index_services_math = -1;
        if (!query.next())
        {
            query.exec("INSERT INTO services (server_index,service_name,service_identation,service_system_name,service_is_deleted) "
                       "VALUES(0,'Математические',"+QString::number(service_index_function_c+1)+",'QtScript',0);");
            index_services_math = query.lastInsertId().toInt();
            //удаляем для записи заново чтобы связать id вновь созданного сервиса
            query.exec("DELETE FROM functions WHERE command_name='++' ");

        }
        else  index_services_math = query.value(0).toInt();


        query.exec( "SELECT id FROM functions WHERE command_name = '++'");
        if (!query.next())
        {
            query.exec("INSERT INTO functions (service_index,command_duration,command_function_id,command_about,command_name,script) "
                       "VALUES("+ QString::number(index_services_math)+", 0 , 0 , "+
                       "'Прибавляет ко входному числу 1 и выводит результат.',"+
                       "'++','out[1]=a0+1;');");

            int last_index = query.lastInsertId().toInt();

            query.exec("INSERT INTO  parameters  (command_index , parameter_number , parameters_name , parameter_name , parameter_value, parameters_type,parameter_type ,inout_type)"
                       "VALUES ("+QString::number(last_index)+",0,'Число','','','Number','',0)");

            query.exec("INSERT INTO  parameters  (command_index , parameter_number , parameters_name , parameter_name , parameter_value, parameters_type,parameter_type ,inout_type)"
                       "VALUES ("+QString::number(last_index)+",0,'Результат','','','Number','',1)");
        }

        query.exec( "SELECT id FROM parameter_types WHERE type = 'String'");
        if (!query.next())
        {
            query.exec("INSERT INTO  parameter_types  (type) VALUES ('String')");
        }
        query.exec( "SELECT id FROM parameter_types WHERE type = 'Number'");
        if (!query.next())
        {
            query.exec("INSERT INTO  parameter_types  (type) VALUES ('Number')");
        }
    }



}
//-------------подключение базы SQLITE -------------------
bool DataBase::setSqliteCachePath(QString dbPath)
{
    QFile sqldbFile;

    sqldbFile.setFileName(dbPath);

    if (!sqldbFile.exists())
    {
        bool result = sqldbFile.open(QIODevice::WriteOnly);
        sqldbFile.close();
        if (!result)
            return false;
    }

    db = QSqlDatabase::addDatabase(bd_type,"db");
    db.setDatabaseName(dbPath);
    if (db.open())
    {
        return true;

    }
    else
        return false;

}


//-------------подключение базы POSTGRES -------------------
bool DataBase::PostgresConnect()
{
    db = QSqlDatabase::addDatabase("QPSQL","db");
    db.setDatabaseName(BD_NAME);
    db.setHostName(IP_BD);
    db.setUserName("postgres");
    db.setPassword("postgres");
    db.setPort(Port_BD);
    db.setConnectOptions("connect_timeout=1");


    timer_status=new QTimer;
    connect(timer_status,SIGNAL(timeout()),SLOT(slot_status()),Qt::QueuedConnection);
    timer_status->start(100);
    if (db.open())
    {

        return true;

    }
    else
        return false;
}
// --------- Возвращает ошибку драйвера
QString DataBase::erororConnect()
{
    QString error1=db.lastError().databaseText();
    QString error2=db.lastError().driverText();

    return error1+" "+error2;
}

bool DataBase::bdConnect(QString type, QString ip,QString name,int port,QString username, QString password)
{
    if (type == "QPSQL"){
        return PostgresConnect();
    }
    if (type == "QSQLITE"){

        return setSqliteCachePath(QCoreApplication::applicationDirPath()+"/db.sqlite");

    }
}

void DataBase::slot_reconnect(QString type, QString ip,QString name,int port,QString username, QString password)
{
    if(db.isOpen())db.close();

    BD_NAME = name;
    IP_BD = ip;
    Port_BD = port;
    bd_type = type;
    UserName = username;
    Password = password;


    if (bdConnect( type,  ip, name, port, username,  password))
    {
        CreateTables(); //создание таблиц бд
        get_max_IdElements();
        emit sig_bd_open(true); //база открыта
        slot_load_scripts();
    }
    else{
        emit sig_bd_open(false);
        QMessageBox msgBox;
        msgBox.setText("DataBase not opened");
        msgBox.setInformativeText(erororConnect());
        msgBox.exec();
    }



}



//--------------------------------Сохраниение глобальных переменных -------------------------------
void DataBase::save_global_values()
{
    QSqlQuery query(QSqlDatabase::database("db"));
    QString sql="DELETE FROM global_parameters";
    bool rez=query.exec(sql);

    foreach (Parameter* val, GlobalValues::Values)
    {
        QString sql="INSERT INTO  global_parameters  (name, type , value )"
                    "VALUES ('"+val->parameters_name+"','"+val->parameters_type+"','"+val->value+"')";
        bool rez=query.exec(sql);
    }
}

//--------------------------------Добавление нового узла -------------------------------
void DataBase::slot_element_add(ScriptElement *el)
{
    QSqlQuery query(QSqlDatabase::database("db"));
    QString sql="INSERT INTO  script_element  (id_element ,id_script , x , y , id_service ,id_command ,  script , comment)"
                "VALUES ("+QString::number(el->id)+","+QString::number(el->id_script)+","+
            QString::number(el->point.x())+"," +QString::number(el->point.y())+"," +QString::number(el->service.service_id)+
            "," +QString::number(el->function.id)+",'"+el->function.script+"','"+el->function.script+"')";
    bool rez=query.exec(sql);


    foreach (Parameter parameter, el->parameters) {
        sql="INSERT INTO  script_parameters  (id_element , parameter_name , value , id_element_connect , parameters_name_connect )"
            "VALUES ("+QString::number(el->id)+",'"+parameter.parameters_name+"','',-1, '')";
        bool rez=query.exec(sql);
    }

    /* sql="INSERT INTO  script_parameters  (id_element , parameter_name , value , id_element_connect , parameters_name_connect )"
        "VALUES ("+QString::number(el->id)+",'Выход0','',-1, '')";*/
    // query.exec(sql);
}

//--------------------------------Изменение информации узла -------------------------------
void DataBase::slot_element_change(ScriptElement *el)
{
    QSqlQuery query(QSqlDatabase::database("db"));
    QString sql="UPDATE script_element SET x="+QString::number(el->point.x())+
            ",y="+QString::number(el->point.y())+
            ",id_service="+QString::number(el->service.service_id)+
            //",service_number="+QString::number(el->service_identation)+
            ",id_command="+QString::number(el->function.id)+
            ",script='"+el->function.script+"'"+
            ",comment='"+el->comment+"'"+
            " WHERE id_element="+QString::number(el->id);
    bool rez=query.exec(sql);

    foreach (Parameter parameter, el->parameters) {
        QString sql;

        sql="UPDATE script_parameters SET value='"+parameter.value+"', id_element_connect = "+QString::number(parameter.id_element_connect)+","+
                " parameters_name_connect ='"+ parameter.parameters_name_connect +"' WHERE id_element="+QString::number(el->id)+" AND parameter_name='"+parameter.parameters_name+"'";



        bool rez=query.exec(sql);
    }
}

//--------------------------------Удаление  узла -------------------------------
void DataBase::delete_element(ScriptElement *el)
{
    QSqlQuery query(QSqlDatabase::database("db"));
    QString sql="DELETE FROM script_element WHERE id_element="+QString::number(el->id);
    bool rez=query.exec(sql);

    foreach (Parameter parameter, el->parameters) {
        QString sql="DELETE FROM script_parameters WHERE id_element="+QString::number(el->id)+" AND parameter_name='"+parameter.parameters_name+"'";
        bool rez=query.exec(sql);
    }
}

//--------------------------------Изменение позиции  узла -------------------------------
void DataBase::slot_element_set_pos(int id_element,QPoint point)
{
    QSqlQuery query(QSqlDatabase::database("db"));
    QString sql= "UPDATE script_element SET x="+QString::number(point.x())+", y="+QString::number(point.y())+" WHERE id_element="+QString::number(id_element);
    bool rez=query.exec(sql);

}


//--------------------------------Изменение значения  параметра -------------------------------
void DataBase::slot_changeParameterValue(ScriptElement *element,int num)
{

    QSqlQuery query(QSqlDatabase::database("db"));

    Parameter parameter = element->parameters.value(num);

    QString sql="UPDATE script_parameters SET value='"+parameter.value+"', id_element_connect = "+QString::number(parameter.id_element_connect)+","+
            " parameters_name_connect ='"+ parameter.parameters_name_connect +"' WHERE id_element="+QString::number(element->id)+" AND parameter_name='"+parameter.parameters_name+"'";
    bool rez=query.exec(sql);

    //         int gg=0;
    //         QFile file("out.txt");
    //         if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
    //             return;

    //         QTextStream out(&file);
    //         out << sql <<  "\n";
    //         file.close();
}

//---------------------------------удаление сценария со связями
void DataBase::removeScript(QString name)
{
    Script *script = Scripts.value(name);
    if (script){
        QSqlQuery query(QSqlDatabase::database("db"));
        foreach (ScriptElement* element, script->elemens) {
            if (element->id){
                QString sql="DELETE FROM script_element WHERE id_element="+QString::number(element->id);
                query.exec(sql);
                foreach (Parameter parameter, element->parameters) {
                    QString sql="DELETE FROM script_parameters WHERE id_element="+QString::number(element->id)+" AND parameter_name='"+parameter.parameters_name+"'";
                    query.exec(sql);
                }
                delete element;
            }
            script->elemens.clear();
        }
        delete script;
        Scripts.remove(name);
    }


}

void DataBase::removeService(int id)
{
    QSqlQuery query(QSqlDatabase::database("db"));
    query.exec( "DELETE FROM  services WHERE service_id="+QString::number(id));
    QList <Function> functions;
    if (get_functions_byServiceId(id,functions)){
        foreach (Function function, functions) {
            removeFunction(function.id);
        }
    }



}

void DataBase::removeFunction(int id)
{
    QSqlQuery query(QSqlDatabase::database("db"));
    query.exec( "DELETE FROM  functions WHERE id="+QString::number(id));
    query.exec( "DELETE FROM  parameters WHERE command_index="+QString::number(id));

}

void DataBase::removeParameter(int id)
{
    QSqlQuery query(QSqlDatabase::database("db"));
    query.exec( "DELETE FROM  parameters WHERE id="+QString::number(id));

}

//удаление глобальной переменной
void DataBase::removeGlobalParameter(QString name)
{
    QSqlQuery query(QSqlDatabase::database("db"));
    query.exec( "DELETE FROM  global_parameters WHERE name='"+name+"'");

}

//-----------------------------------изменение функции -----------------------------------------
void DataBase::slot_save_function_to_bd(ScriptElement *element)
{
    QSqlQuery query(QSqlDatabase::database("db"));


    int service_identation= -1;
    //ищем сервис по имени и если не находим добавляем новый
    query.exec( "SELECT service_id,service_identation FROM services WHERE service_name = '"+element->service.service_name+"'");
    if (query.next())
    {
        element->function.service_index= query.value(0).toInt();
        service_identation= query.value(1).toInt();
    }
    else
    {
        bool result = query.exec("INSERT INTO services (server_index,service_name,service_identation,service_system_name,service_is_deleted) "
                                 "VALUES(0,'"+element->service.service_name+"',"+QString::number(service_index_function_c)+",'QtScript',0);");

        element->function.service_index=query.lastInsertId().toInt(); //получение вставленного ID функции
    }



    int command_index = -1;
    if (element->function.id!=-1)
    {
        bool rez=query.exec( "UPDATE functions SET script='"+element->function.script+"', "+
                             "command_about='"+element->function.comand_about+"', "+
                             "command_name='"+element->function.comand_name+"' "+
                             " WHERE id="+QString::number(element->function.id));

        //удаляем все параметры этой функции
        query.exec( "DELETE FROM  parameters WHERE command_index="+QString::number(element->function.id));

        command_index = element->function.id;
    }
    else  //значит создаем новую функцию
    {
        bool result = query.exec("INSERT INTO functions (service_index,command_duration,command_function_id,command_about,command_name,script) "
                                 "VALUES("+QString::number(element->function.service_index)+", 60 , 0 , "+
                                 "'"+element->function.comand_about+"',"+
                                 "'"+element->function.comand_name+"',"+
                                 "'"+element->function.script+"');");

        command_index=query.lastInsertId().toInt(); //получение вставленного ID функции
    }



    //вставляем параметры
    if (command_index!=-1){

        foreach (Parameter parameter, element->parameters) {
            if (parameter.inout_type == 1 && parameter.parameters_name == "Выход")
                continue;
            QString sql="INSERT INTO  parameters  (command_index , parameter_number , parameters_name , parameter_name , parameter_value, parameters_type,parameter_type ,inout_type)"
                        "VALUES ("+QString::number(command_index)+","
                    +QString::number(parameter.parameter_number)+","+
                    "'"+parameter.parameters_name+"',"+
                    "'"+parameter.parameter_name+"',"+
                    "'"+parameter.parameter_value+"',"+
                    "'"+parameter.parameters_type+"',"+
                    "'"+parameter.parameter_type+"',"
                    +QString::number(parameter.inout_type)+")";

            bool rez=query.exec(sql);
        }
    }

    emit sig_updateWidgetFunctions(); //обновим дерево функций


}









void DataBase::save_function_to_bd(int id_element, QString text)
{
    QSqlQuery query(QSqlDatabase::database("db"));
    bool rez=query.exec( "UPDATE script_element SET function='"+text+"' WHERE id_element="+QString::number(id_element));
}

// ----------- загрузка всех скриптов из БД ----------------
void DataBase::slot_load_scripts()
{


    QSqlQuery query(QSqlDatabase::database("db"));
    QString sql="SELECT name, type , value FROM global_parameters";
    bool rez=query.exec(sql);
    while(query.next())
    {
        Parameter* val = new Parameter;
        val->parameters_name = query.value(0).toString();
        val->parameters_type = query.value(1).toString();
        val->value = query.value(2).toString();
        GlobalValues::Values.append(val);
    }

    emit sig_read_global_values(); //добавляем глобальные переменные на форму


    Scripts = read_scripts();
    foreach (QString name, Scripts.keys()) {
        Script* script = Scripts.value(name);
        script->is_new = false;
        script->is_start = false;
        //script->is_update = false;
        QSqlQuery query(QSqlDatabase::database("db"));
        bool rez=query.exec("SELECT id_element,x,y,id_service,id_command,script,comment FROM script_element WHERE id_script="+QString::number(script->id));
        if (rez)
        {
            while(query.next())
            {
                ScriptElement *element=new ScriptElement;

                element->id = query.value(0).toInt();
                element->point.setX(query.value(1).toInt());
                element->point.setY(query.value(2).toInt());

                element->is_new = false;
                element->is_update = false;
                element->is_deleted = false;

                int id_service = query.value(3).toInt();

                if (get_service_byId(id_service,element->service))
                {
                    Server server;
                    if (get_serverById(element->service.server_index,server))
                    {
                        element->service.server_ip = server.server_ip;
                    }
                    else
                    {
                        element->service.server_ip = "localhost";
                    }


                    int id_funct = query.value(4).toInt();
                    if (get_function_byFunctionId(element->service.service_id, id_funct, element->function))
                    {
                        get_parameters_byServiceFunctionIdentation(element->function.id, element->parameters);
                    }
                }
                set_parameters_value_byElementId(element);

                element->function.script =  query.value(5).toString();

                element->comment= query.value(6).toString();
                script->elemens.insert(element->id,element);

            }



        }

    }


    emit sig_bd_scripts_loaded(); //загружаем первый скрипт в окне редактора


}

//------------------------сохранить все в базу данных
void DataBase::slot_save()
{

    foreach (QString name, Scripts.keys()) {
        Script* script = Scripts.value(name);


        foreach (ScriptElement* el, script->elemens) {
            if (el->is_new)
            {
                slot_element_add(el) ;
                el->is_new = false;
            }
            if (el->is_update)
            {
                slot_element_change(el) ;
                el->is_update = false;
            }
            if (el->is_deleted)
            {
                delete_element(el) ;
                el->is_deleted = false;
                script->elemens.remove(el->id);
                delete el;

            }
        }

        save_global_values();



    }

}



//--------- Получить все скрипты из бд
QMap <QString, Script* > DataBase::read_scripts()
{
    QMap <QString, Script* > scripts;
    if (QSqlDatabase::database("db").isOpen()){
        QSqlQuery query(QSqlDatabase::database("db"));
        if (query.exec("SELECT id, name, comment FROM scripts"))
        {
            while(query.next())
            {
                Script* script = new Script;
                script->id = query.value(0).toInt();
                script->name = query.value(1).toString();
                script->comment = query.value(2).toString();
                scripts.insert(script->name,script);
            }
        }
    }
    return scripts;
}


void DataBase::read_services()
{

}



//--------------- вытащить службу из запроса ---------------------
Service DataBase::get_service_byQuery(QSqlQuery query)
{
    Service service;
    service.service_id = query.value(0).toInt();
    service.server_index = query.value(1).toInt();
    service.service_name= query.value(2).toString().simplified();
    service.service_identation= query.value(3).toInt();
    service.service_system_name = query.value(4).toString().simplified();
    service.service_is_deleted = query.value(5).toBool();
    return service;
}
//--------------- Собрать все службы из запроса ---------------------
bool DataBase::get_service_byQuery(QSqlQuery query, QList<Service>& services)
{
    bool result=false;
    while(query.next())
    {
        Service service = get_service_byQuery(query);
        services.append(service);
        result = true;
    }
    return result;
}

// ---------------------получить все службы ---------------------------------
bool DataBase::get_services(QList<Service>& services)
{
    bool result=false;
    if (QSqlDatabase::database("db").isOpen()){
        QSqlQuery query(QSqlDatabase::database("db"));
        if (query.exec("SELECT service_id, server_index, service_name, service_identation,service_system_name, service_is_deleted FROM services"))
        {
            result = get_service_byQuery(query, services);
        }
    }
    return result;
}

bool DataBase::get_service_byId(int id,Service& service)
{
    bool result=false;
    if (QSqlDatabase::database("db").isOpen()){
        QSqlQuery query_service(QSqlDatabase::database("db"));
        bool rez_service=query_service.exec("SELECT service_id, server_index, service_name, service_identation,service_system_name, service_is_deleted FROM services WHERE service_id="+QString::number(id));
        if (rez_service)
        {
            if(query_service.next())
            {
                service = get_service_byQuery(query_service);
                result=true;
            }
        }
    }

    return result;
}

bool DataBase::get_service_byIdentation(int service_identation, Service& service)
{
    bool result=false;
    if (QSqlDatabase::database("db").isOpen()){
        QSqlQuery query_service(QSqlDatabase::database("db"));
        bool rez_service=query_service.exec("SELECT  service_id, server_index, service_name, service_identation,service_system_name, service_is_deleted FROM services WHERE service_identation="+QString::number(service_identation));
        if (rez_service)
        {
            if(query_service.next())
            {
                service = get_service_byQuery(query_service);
                result=true;
            }
        }
    }

    return result;
}

bool DataBase::service_bySystemName(QString service_system_name, Service& service)
{
    bool result=false;
    if (QSqlDatabase::database("db").isOpen()){
        QSqlQuery query_service(QSqlDatabase::database("db"));
        result=true;
    }

    return result;
}

bool DataBase::service_byServiceName(QString service_name,Service& service)
{
    bool result=false;
    if (QSqlDatabase::database("db").isOpen()){
        QSqlQuery query_service(QSqlDatabase::database("db"));
        result=true;
    }

    return result;
}


//--------------- вытащить функцию из запроса ---------------------
Function DataBase::get_function_byQuery(QSqlQuery query)
{
    Function function;
    function.id= query.value(0).toInt();
    function.service_index= query.value(1).toInt();
    function.comand_name= query.value(2).toString().simplified();
    function.comand_function_id= query.value(3).toInt();
    function.comand_is_deleted= query.value(4).toBool();
    function.comand_about= query.value(5).toString();
    function.script = query.value(6).toString();
    return function;
}
//--------------- Собрать все функции из запроса ---------------------
bool DataBase::get_functions_byQuery(QSqlQuery query, QList<Function>& functions)
{
    bool result=false;
    while(query.next())
    {
        Function function = get_function_byQuery(query);
        functions.append(function);
        result=true;
    }
    return result;
}


//--------------- Получить список всхе функций из БД ---------------------
bool DataBase::get_functions(QList<Function>& functions)
{
    bool result=false;
    if (QSqlDatabase::database("db").isOpen()){
        QSqlQuery query_service(QSqlDatabase::database("db"));
        result=true;
    }

    return result;
}

//------------------- Получить список функций службы по ID службы ------------------------
bool DataBase::get_functions_byServiceId(int service_id,QList<Function>& functions)
{
    bool result=false;
    if (QSqlDatabase::database("db").isOpen()){
        QSqlQuery query(QSqlDatabase::database("db"));
        if (query.exec("SELECT id,service_index,command_name,command_function_id,command_is_deleted,command_about,script FROM functions WHERE service_index="+QString::number(service_id)))
        {
            result = get_functions_byQuery(query,functions);
        }
    }

    return result;
}

//------------------- Получить  функцию службы по ID службы и  ID функции ------------------------
bool DataBase::get_function_byFunctionId(int service_identation, int comand_function_id,Function &function)
{
    bool result=false;
    if (QSqlDatabase::database("db").isOpen()){
        QSqlQuery query(QSqlDatabase::database("db"));
        if (query.exec("SELECT id,service_index,command_name,command_function_id,command_is_deleted,command_about,script "
                       "FROM functions WHERE service_index="+QString::number(service_identation)+" AND id="+QString::number(comand_function_id)))
        {
            if(query.next())
            {
                function = DataBase::get_function_byQuery(query);
                result=true;
            }
        }
    }
    return result;
}

//------------------- Получить  функцию службы по ID службы и  ID функции ------------------------
bool DataBase::get_function_byName(int service_identation, QString comand_name,Function &function)
{
    bool result=false;
    if (QSqlDatabase::database("db").isOpen()){
        QSqlQuery query(QSqlDatabase::database("db"));
        if (query.exec("SELECT id,service_index,command_name,command_function_id,command_is_deleted,command_about,script "
                       "FROM functions WHERE service_index="+QString::number(service_identation)+" AND command_name='"+comand_name+"'"))
        {
            if(query.next())
            {
                function = DataBase::get_function_byQuery(query);
                result=true;
            }
        }
    }
    return result;
}


//--------------- вытащить команду из запроса ---------------------
Parameter DataBase::get_parameter_byQuery(QSqlQuery query)
{
    Parameter par;


    par.id= query.value(0).toInt();
    par.comand_index= query.value(1).toInt();
    par.parameter_number= query.value(2).toInt();
    par.parameters_name= query.value(3).toString().simplified();
    par.parameter_name= query.value(4).toString().simplified();
    par.parameter_value= query.value(5).toString().simplified();
    par.parameters_type= query.value(6).toString().simplified().toLower();
    par.parameter_type= query.value(7).toString().simplified().toLower();
    par.inout_type= query.value(8).toInt();
    par.parameters_name_connect = "";
    par.id_element_connect = -1;
    par.value = "";
    par.is_delete = false;

    return par;
}


//----------------------- добавление выхода или входа функции
void DataBase::AppendOuterParameter(QList<Parameter>& parameters , int type)
{
    Parameter par;
    par.id= -1;
    par.comand_index= -1;
    par.parameter_number= -1;
    if (type == 0){
        par.parameters_name= "Вход";
        par.parameters_type= "connector_in";

    }
    if (type == 1){
        par.parameters_name= "Выход";
        par.parameters_type= "connector_out";
    }
    par.parameter_name= "";
    par.parameter_value= "";

    par.parameter_type= "default"; //коннектор по умолчанию
    par.inout_type= type;
    par.parameters_name_connect = "";
    par.id_element_connect = -1;
    par.value = "";
    parameters.append(par);
}
//--------------- Собрать все команды из запроса ---------------------
bool DataBase::get_parameters_byQuery(QSqlQuery query, QList<Parameter>& parameters, bool append_outers)
{
    bool result=false;
    bool outers = false;
    while(query.next())
    {
        Parameter parameter = get_parameter_byQuery(query);
        parameters.append(parameter);
        if (parameter.inout_type == 1  && parameter.parameters_type.contains("connector"))
            outers = true;

        result=true;
    }
    if (result && append_outers){
        AppendOuterParameter(parameters, 0);  //добавляем вход ко всем функциям

        if (!outers)//добавляем выход к стандартным функциям если нет других выходов
            AppendOuterParameter(parameters, 1);

    }

    return result;
}



// ----------------- считываем заданные параметрам значения
void DataBase::set_parameters_value_byElementId(ScriptElement *el)
{
    if (QSqlDatabase::database("db").isOpen()){
        QSqlQuery query(QSqlDatabase::database("db"));
        if (query.exec("SELECT value, parameter_name,id_element_connect, parameters_name_connect FROM script_parameters WHERE id_element="+QString::number(el->id)))
        {
            while(query.next())
            {
                QString value= query.value(0).toString();
                QString parameter_name= query.value(1).toString();
                int id_element_connect= query.value(2).toInt();
                QString parameters_name_connect= query.value(3).toString();

                if ((el->function.comand_name == "Получить" || el->function.comand_name == "Установить"))
                {
                    //для работы с глобальными переменными мы заменяем имя параметра на имя глобальной переменной
                    int num = 0;
                    foreach (Parameter par, el->parameters) {
                        if (par.parameters_name == "Параметр"){
                            el->parameters[num].parameters_name = parameter_name;
                        }
                        num++;
                    }

                }
                else{

                    int num = 0;
                    foreach (Parameter par, el->parameters) {
                        if (par.parameters_name == parameter_name){
                            el->parameters[num].value = value;
                            el->parameters[num].id_element_connect = id_element_connect;
                            el->parameters[num].parameters_name_connect = parameters_name_connect;
                            break;
                        }
                        num++;
                    }
                }




            }
        }

    }

}

//------------------- Получить список  функций  по ID службы и ID функции------------------------
bool DataBase::get_parameters_byServiceFunctionIdentation(int command_function_id, QList<Parameter> &parameters,bool append_outers)
{
    bool result=false;
    if (QSqlDatabase::database("db").isOpen()){
        QSqlQuery query(QSqlDatabase::database("db"));
        if (query.exec("SELECT id,command_index,parameter_number,parameters_name,parameter_name,parameter_value,parameters_type,parameter_type,inout_type"
                       " FROM parameters WHERE command_index="+QString::number(command_function_id)))
        {
            result = DataBase::get_parameters_byQuery(query,parameters,append_outers);
        }
    }
    return result;
}


void DataBase::getServerFromQuery(QSqlQuery query, Server server)
{

    server.server_id= query.value(0).toInt();
    server.server_name= query.value(1).toString();
    server.server_number= query.value(2).toInt();
    server.server_ip= query.value(3).toString();
    server.server_is_deleted= query.value(4).toBool();


}

//получить ip адресс сервера на котором установлена служба по его id
bool DataBase::get_serverById(int server_id,Server &server)
{
    bool result=false;
    if (QSqlDatabase::database("db").isOpen()){
        QSqlQuery query(QSqlDatabase::database("db"));
        if (query.exec("SELECT server_id,server_name,server_number,server_ip,server_is_deleted"
                       " FROM servers WHERE server_id="+QString::number(server_id)))
        {
            if(query.next())
            {
                getServerFromQuery(query,server);
                result = true;
            }
        }
    }

    return result;
}
//получить ip адресс сервера на котором установлена служба по его номеру
bool DataBase::get_serverByNumber(int server_number, Server &server)
{
    bool result=false;
    if (QSqlDatabase::database("db").isOpen()){
        QSqlQuery query(QSqlDatabase::database("db"));
        if (query.exec("SELECT server_id,server_name,server_number,server_ip,server_is_deleted"
                       " FROM servers WHERE server_number="+QString::number(server_number)))
        {
            if(query.next())
            {
                getServerFromQuery(query,server);
                result = true;
            }
        }
    }

    return result;
}

void DataBase::appendScript(Script *script)
{
    Scripts.insert(script->name,script);
}

int DataBase::getScriptIdByName(Script *script)
{
    bool result=false;
    if (QSqlDatabase::database("db").isOpen()){
        QSqlQuery query(QSqlDatabase::database("db"));
        if (query.exec("SELECT id FROM scripts WHERE name='"+script->name+"'"))
        {
            if(query.next())
            {
                return query.value(0).toInt();
            }
        }
    }
    return -1;
}






