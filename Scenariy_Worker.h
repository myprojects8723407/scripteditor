#ifndef SERVICE51_H
#define SERVICE51_H

//класс выполнения сценария без службы 51

#include <QObject>
#include <QThread>
#include <QTimer>
#include <QtScript>
#include "Structures.h"


class GlobalValues;
class DataBase;

class ScriptWorker : public QThread
{
    Q_OBJECT
public:
    explicit ScriptWorker(QObject *parent = 0);
    void setScript(Script *script, int id_start_element);
    void run();

    bool exit = false;

signals:
    void sig_state(int state, QString error_name,int id);
    void sig_state(Script*,int,QString,int);
    void sig_load_script(Script *script, QString name,int id_start,int tek_element_id);

public slots:
    void slot_start_script(Script *,int id_start);

private:
    Script *script;
    ScriptElement *tek_element;
    int id_start_element;

    void AppendValue(QScriptValueList &list, QList<QVariant> &list_variant, Parameter* var);
};

#endif // SERVICE51_H
