#include "ScriptWorkerManager.h"
#include "DataBase.h"

ScriptWorkerManager::ScriptWorkerManager(QObject *parent) : QObject(parent)
{

}
//------------- создание и запуск скрипта
void ScriptWorkerManager::slot_StartScript(Script *script, int id_start_element)
{
    ScriptWorker* script_worker=new ScriptWorker();
    script_worker->setScript(script, id_start_element);
    scripts.insert(script,script_worker);
    connect(script_worker, SIGNAL(sig_state(Script*,int,QString,int)),this, SIGNAL(sig_state(Script*,int,QString,int)), Qt::QueuedConnection);
    connect(script_worker, SIGNAL(sig_load_script(Script*,QString,int,int)),this, SLOT(slot_load_script(Script*,QString,int,int)), Qt::QueuedConnection);

    script_worker->start();

}
//------------ остановка выполнения потока скрипта
void ScriptWorkerManager::slot_StopScript(Script *script)
{
    ScriptWorker* script_worker = scripts.value(script);
    if (script_worker)
    {
        script_worker->exit = true;
    }
}
//----------------- Запуск другого скрипта по срабатыванию функции
void ScriptWorkerManager::slot_load_script(Script *script, QString name, int id_start, int tek_element_id)
{
    ScriptWorker* script_worker = scripts.value(script);
    if (script_worker)
    {
        Script *script_new = DataBase::Scripts.value(name);
        if (script_new && !script_new->is_start) //если не выполняется
        {
            script_new->is_start = true; //начал выполнение
            script_worker->exit = false;
            script_worker->setScript(script_new, id_start);
            script_worker->start();
        }
    }
}
