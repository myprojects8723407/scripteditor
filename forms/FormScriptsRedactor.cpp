#include "FormScriptsRedactor.h"
#include "GlobalValues.h"

FormScriptRedactor::FormScriptRedactor(QWidget *parent) : QMainWindow(parent)
{
    form_tree_view = 0;


    this->setWindowTitle("Редактирование сценария работ");
    start_icon=QIcon("://image/new/start_scenariy.png");
    stop_icon=QIcon("://image/new/pause.png");
    act_script_start_stop = new QAction("Запустить сценарий", this);
    act_script_start_stop->setIcon(start_icon);
    act_script_start_stop->setCheckable(true);
    connect(act_script_start_stop,SIGNAL(triggered(bool)),SLOT(slot_start_script(bool)));

    form_add_script=new FormAddScenariy();

    QAction * act_script_append = new QAction(QIcon("://image/new/plus.png"), "Добавить сценарий", this);
    QAction * act_script_remove = new QAction(QIcon("://image/new/remove.png"), "Удалить сценарий", this);
    QAction * act_script_save = new QAction(QIcon(":/images/image/new/save.png"), "Сохранить всё", this);
    QAction* reload_script = new QAction(QIcon("://image/new/reload.png"), "Перезагрузить скрипт", this);
    QAction* show_database = new QAction(QIcon("://image/new/list.png"), "Редактировать БД", this);
    connect(reload_script, SIGNAL(triggered(bool)),this, SLOT(slot_reload_current_script()), Qt::QueuedConnection);

    formdatabase = new  FormDataBase;
    connect(show_database, SIGNAL(triggered(bool)),formdatabase, SLOT(show()), Qt::QueuedConnection);
    connect(formdatabase, SIGNAL(sig_updateWidgetFunctions()),this, SLOT(slot_updateWidgetFunctions()), Qt::QueuedConnection);


    connect(act_script_save,SIGNAL(triggered(bool)),this,SIGNAL(sig_save()));

    //    form_scan_services=new FormScanServices;
    //    reload_services = new QAction(QIcon("://image/new/reload.png"), "Обновить список служб и операций", this);
    //    connect(reload_services, SIGNAL(triggered(bool)),form_scan_services, SLOT(show()), Qt::QueuedConnection);

    global_value = new QTableWidget;
    global_value->verticalHeader()->hide();

    view_script_redactor=new ViewScriptRedaktor;
    this->setCentralWidget(view_script_redactor);
    connect(this, SIGNAL(sig_start_script()),view_script_redactor, SLOT(slot_start_script()), Qt::QueuedConnection);
    connect(this, SIGNAL(sig_stop_script()),view_script_redactor, SLOT(slot_stop_script()), Qt::QueuedConnection);



    act_add_function = new QAction(QIcon("://image/new/function.png"), "Создать функцию", this);
    connect(act_add_function,SIGNAL(triggered(bool)),view_script_redactor->formEditFunction,SLOT(slot_newFunction()));

    formScripts=new FormScripts; //форма редактирования и просмотра сценария
    formScripts->setDragDropMode(QAbstractItemView::DragDrop);
    formScripts->setDragDropOverwriteMode(true);
    formScripts->setDropIndicatorShown(true);
    //connect(redactor->act_script_change, SIGNAL(triggered(bool)),redactor_script, SLOT(show()), Qt::QueuedConnection);

    connect(form_add_script,SIGNAL(sig_primenit(QString,QString)),formScripts,SLOT(slot_append_script(QString,QString)));


    QToolBar *toolbar = new QToolBar(this);
    toolbar->addAction(act_script_save);
    //toolbar->addAction(act_script_append);
    toolbar->addAction(reload_script);
    toolbar->addAction(act_script_start_stop);
    toolbar->addAction(act_add_function);
    toolbar->addAction(show_database);

    //toolbar->addAction(reload_services);
    this->addToolBar(toolbar);

    listWidgetParamreters = new ListWidgetParamreters;

    QWidget *hbox_w_scripts =new QWidget;
    QHBoxLayout *hbox_scripts  =new QHBoxLayout;
    QPushButton *button_add_scripts  = new QPushButton("+");
    QPushButton *button_remove_scripts  = new QPushButton("-");
    hbox_scripts->addWidget(button_add_scripts );
    hbox_scripts->addWidget(button_remove_scripts );
    hbox_w_scripts->setLayout(hbox_scripts);
    connect(button_add_scripts,SIGNAL(clicked(bool)),form_add_script,SLOT(show()));
    connect(button_remove_scripts,SIGNAL(clicked(bool)),formScripts,SLOT(slot_remove_script()));


    QWidget *hbox_w_value =new QWidget;
    QHBoxLayout *hbox_value =new QHBoxLayout;
    QPushButton *button_add_value = new QPushButton("+");
    QPushButton *button_remove_value = new QPushButton("-");
    QPushButton *button_update_value = new QPushButton("<>");
    hbox_value->addWidget(button_add_value);
    hbox_value->addWidget(button_remove_value);
    hbox_value->addWidget(button_update_value);
    hbox_w_value->setLayout(hbox_value);
    connect(button_add_value,SIGNAL(clicked(bool)),this,SLOT(slot_add_value()));
    connect(button_remove_value,SIGNAL(clicked(bool)),this,SLOT(slot_remove_value()));
    connect(button_update_value,SIGNAL(clicked(bool)),this,SLOT(slot_update_values_to_form()));

    QStringList horzHeaders;
    horzHeaders << "Имя" << "Тип" << "Значение";
    global_value->setColumnCount(3);
    global_value->setHorizontalHeaderLabels( horzHeaders );


    listWidgetParamreters->setHorizontalHeaderLabels( horzHeaders );

    QToolBar *toolbar_script_name = new QToolBar(this);
    toolbar_script_name->addWidget(hbox_w_scripts);
    toolbar_script_name->addWidget(formScripts);
    toolbar_script_name->addWidget(hbox_w_value);
    toolbar_script_name->addWidget(global_value);
    toolbar_script_name->addWidget(listWidgetParamreters);
    this->addToolBar(Qt::LeftToolBarArea,toolbar_script_name);


    connect(view_script_redactor, SIGNAL(sig_set_parameters_to_listWidget(ScriptElement*)),listWidgetParamreters, SLOT(slot_set_parameters_to_listWidget(ScriptElement*)), Qt::QueuedConnection);
    connect(listWidgetParamreters, SIGNAL(sig_parameter_change(ScriptElement*)),view_script_redactor, SLOT(slot_parameter_change(ScriptElement*)), Qt::QueuedConnection);
    connect(view_script_redactor, SIGNAL(sig_reload_current_script()),formScripts, SLOT(slot_reload_current_script()), Qt::QueuedConnection);

    connect(view_script_redactor,SIGNAL(sig_script_read_from_bd(Script*)),SLOT(slot_script_read_from_bd(Script*)),Qt::QueuedConnection);
    connect(global_value,SIGNAL(currentItemChanged(QTableWidgetItem*,QTableWidgetItem*)),SLOT(slot_currentItemChanged(QTableWidgetItem*,QTableWidgetItem*)),Qt::QueuedConnection);

    //отжатие кнопки старта скрипта
    connect(view_script_redactor,SIGNAL(sig_script_started(bool)),SLOT(slot_change_state_button_start_script(bool)),Qt::QueuedConnection);

    connect(view_script_redactor,SIGNAL(sig_contextMenuEvent(QPoint,QPoint)),SLOT(slot_contextMenuEvent(QPoint,QPoint)),Qt::QueuedConnection);  //создание визуального элемента



    global_value->installEventFilter(this);
    //одиночное выделение
    global_value->setSelectionBehavior( QAbstractItemView::SelectItems );
    global_value->setSelectionMode( QAbstractItemView::SingleSelection );


}


//-------------------------заполнение дерева выбора функций ----------------------------
void FormScriptRedactor::updateWidgetFunctions()
{
    if (form_tree_view) {
        disconnect(form_tree_view,SIGNAL(sig_funct_doubleClick(int,int,QPoint)));
        delete form_tree_view;
    }
    form_tree_view=new FormTreeViewFunctins(this);
    connect(form_tree_view,SIGNAL(sig_funct_doubleClick(int,int,QPoint)),view_script_redactor,SLOT(slot_funct_doubleClick(int,int,QPoint)),Qt::QueuedConnection);


    QList <Service> services;
    if (DataBase::get_services(services)){
        foreach (Service service, services) {
            form_tree_view->appendService(service.service_id,service.service_name);
            QList <Function> functions;
            if (DataBase::get_functions_byServiceId(service.service_id,functions)){
                foreach (Function function, functions) {
                    form_tree_view->appendFunction(function.id,service.service_name,function.comand_name);
                }
            }
        }
    }
}

//------------------показ окна для выбора функции ---------------------------------------
void FormScriptRedactor::slot_contextMenuEvent(QPoint point_on_screen,QPoint point_on_scene)
{
    if (form_tree_view){
        form_tree_view->move(point_on_screen);
        form_tree_view->show();
        form_tree_view->point_on_scene = point_on_scene;
    }
}

void FormScriptRedactor::slot_set_id_last(int id_last)
{
    this->view_script_redactor->setIdLast(id_last);
}

void FormScriptRedactor::slot_script_read_from_bd(Script* script)
{
    view_script_redactor->set_script(script);
}

void FormScriptRedactor::slot_start_script(bool flag)
{
    if (flag){
        slot_change_state_button_start_script(true);
        view_script_redactor->resetFunctions(); //удалить предыдущие результаты работы
        emit sig_start_script();
    }
    else {
        slot_change_state_button_start_script(false);
        emit sig_stop_script();
    }
}

void FormScriptRedactor::slot_change_state_button_start_script(bool flag)
{
    if (flag){
        act_script_start_stop->setChecked(true);
        act_script_start_stop->setIcon(stop_icon);
    }
    else {
        act_script_start_stop->setIcon(start_icon);
        act_script_start_stop->setChecked(false);
    }
}

// перезагрузка текущего скрипта
void FormScriptRedactor::slot_reload_current_script()
{
    formScripts->slot_select_script(formScripts->currentIndex());
}

//--------------------добавляем глобальную переменную
void FormScriptRedactor::slot_add_value()
{

    Parameter* value = new Parameter;
    value->parameters_name = "Параметр"+QString::number(global_value->rowCount());
    GlobalValues::Values.append(value);
    add_global_value(value);

}
//------------------------удаляем глобальную переменную
void FormScriptRedactor::slot_remove_value()
{
    QString name = GlobalValues::Values.value(global_value->currentRow())->parameters_name;
    DataBase::removeGlobalParameter(name);
    delete GlobalValues::Values.value(global_value->currentRow());
    GlobalValues::Values.removeAt(global_value->currentRow());
    global_value->removeRow(global_value->currentRow());
}
//--------------------обновляем переменные на форме после чнения из БД
void FormScriptRedactor::slot_append_global_values_to_form()
{
    foreach (Parameter* value, GlobalValues::Values) {
        add_global_value(value);
    }

}



//--------------------обновляем значения переменных на форме по нажатию на кнопку
void FormScriptRedactor::slot_update_values_to_form()
{

    for (int i=0; i<global_value->rowCount(); i++)
    {
        //если совпадает имя переменной с какой либо другой  то запрещаем менять
        if (global_value->item(i,0)->text() == GlobalValues::Values.value(i)->parameters_name)
        {
            global_value->item(i,2)->setText(GlobalValues::Values.value(i)->value);

        }
    }
}
//------- добавление единичной переменной
void FormScriptRedactor::add_global_value(Parameter* value)
{
    global_value->insertRow(global_value->rowCount());

    QTableWidgetItem* taskItem = new QTableWidgetItem();
    taskItem->setText(value->parameters_name);
    global_value->setItem(global_value->rowCount()-1,0,taskItem);

    taskItem = new QTableWidgetItem();
    taskItem->setText(value->parameters_type);
    global_value->setItem(global_value->rowCount()-1,1,taskItem);
    global_value->setItemDelegateForColumn(1,new MyComboDelegate("parameter_types","id","type"));

    taskItem = new QTableWidgetItem();
    taskItem->setText(value->value);
    global_value->setItem(global_value->rowCount()-1,2,taskItem);


}

//обновление всех переменных
void FormScriptRedactor::changeGlobalValues()
{
    for (int i=0; i<global_value->rowCount(); i++)
    {
        GlobalValues::Values[i]->parameters_name = global_value->item(i,0)->text();
        GlobalValues::Values[i]->parameters_type = global_value->item(i,1)->text();
        GlobalValues::Values[i]->value =global_value->item(i,2)->text();
    }


}
//обновление иформации по одной переменной
void FormScriptRedactor::changeGlobalValue(int num)
{

    for (int i=0; i<global_value->rowCount(); i++)
    {
        //если совпадает имя переменной с какой либо другой  то запрещаем менять
        if (global_value->item(num,0)->text() == global_value->item(i,0)->text() && i!=num)
        {
            global_value->item(num,0)->setText(GlobalValues::Values.value(num)->parameters_name);
            return;
        }
    }

    GlobalValues::Values[num]->parameters_name = global_value->item(num,0)->text();
    GlobalValues::Values[num]->parameters_type = global_value->item(num,1)->text();
    GlobalValues::Values[num]->value =global_value->item(num,2)->text();
}


void FormScriptRedactor::slot_currentItemChanged(QTableWidgetItem *item_current, QTableWidgetItem *last_item)
{
    changeGlobalValues();
}

void FormScriptRedactor::slot_bd_script_loaded()
{
    if (DataBase::Scripts.count()>0)
        view_script_redactor->set_script(DataBase::Scripts.value(DataBase::Scripts.keys().value(0)));
}

//прошло обновление информации в БД обновить на формах
void FormScriptRedactor::slot_updateWidgetFunctions()
{
    //view_script_redactor->updateWidgetFunctions();
        updateWidgetFunctions();
}


bool FormScriptRedactor::eventFilter(QObject *obj, QEvent *event)
{

    if (obj == global_value){
        QKeyEvent* event_key =dynamic_cast<QKeyEvent*>(event);
        if (event_key){
            if (event_key->key() == Qt::Key_Return && event_key->type() == QKeyEvent::KeyRelease){

                changeGlobalValue(global_value->currentRow()) ;

                return true;
            }
        }

        int ev = event->type();
        if (event->type() == QEvent::Leave)
        {
            //курсор покидает облпсть таблицы глобальных параметров
            this->setCursor(QCursor(Qt::ArrowCursor));
            return true;
        }
        if (event->type() == QEvent::FocusIn)
        {
            //нажатие мыши в области таблицы глобальных параметров
            view_script_redactor->global_values_mouse_press = true;
            int row = global_value->currentRow();
            if (row>-1)
                view_script_redactor->nameGlobalValue = global_value->item(row,0)->text();
            this->setCursor(QCursor(Qt::ClosedHandCursor));  //меняем курсор на руку
            return true;
        }
        if (event->type() == QEvent::FocusOut)
        {
            //нажатие мыши вне области таблицы глобальных параметров
            view_script_redactor->global_values_mouse_press = false;
            view_script_redactor->nameGlobalValue = "";
            this->setCursor(QCursor(Qt::ArrowCursor));
            return true;
        }

    }

    return QObject::eventFilter(obj, event);



}


