#ifndef FORMADDSCENARIY_H
#define FORMADDSCENARIY_H

#include <QWidget>
#include <QMessageBox>

class DataBase;


namespace Ui {
class FormAddScenariy;
}

class FormAddScenariy : public QWidget
{
    Q_OBJECT

public:
    explicit FormAddScenariy(QWidget *parent = 0);
    ~FormAddScenariy();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

signals:
     void sig_primenit(QString,QString); //задание названмия сценария

private:
    Ui::FormAddScenariy *ui;
};

#endif // FORMADDSCENARIY_H
