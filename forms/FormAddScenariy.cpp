#include "FormAddScenariy.h"
#include "ui_FormAddScenariy.h"
#include "DataBase.h"

FormAddScenariy::FormAddScenariy(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FormAddScenariy)
{
    ui->setupUi(this);
}

FormAddScenariy::~FormAddScenariy()
{
    delete ui;
}

void FormAddScenariy::on_pushButton_clicked()
{
    QString name=ui->lineEdit_script_name->text();
    QString komment=ui->lineEdit_komment->text();
    if (name.isNull()){
        QMessageBox msg;
        msg.setWindowTitle("Внимание");
        msg.setText("Введите название сценария");
        msg.exec();
    }
    else
    {
        Script* script_new = new Script;
        script_new->name = name;
        script_new->comment = komment;
        script_new->is_new = true;
        script_new->is_start =false;
        script_new->point.setX(0);
        script_new->point.setY(0);

        DataBase::appendScript(script_new);
        emit sig_primenit(name,komment);
    }
    this->close();
}

void FormAddScenariy::on_pushButton_2_clicked()
{
    this->close();
}
