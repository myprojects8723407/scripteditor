#ifndef FORMDATABASE_H
#define FORMDATABASE_H

#include <QWidget>
#include <QTableWidget>
#include <Structures.h>

class DataBase;

namespace Ui {
class FormDataBase;
}

class FormDataBase : public QWidget
{
    Q_OBJECT

public:
    explicit FormDataBase(QWidget *parent = 0);
    ~FormDataBase();
    void ChangeData();

private slots:
    void on_pushButton_delete_service_clicked();

    void on_pushButton_delete_function_clicked();

    void on_pushButton_delete_parameter_clicked();
    void slot_services_cellClicked(int,int);
    void slot_functions_cellClicked(int,int);

protected:

    virtual void  showEvent(QShowEvent *event) override;


private:
    Ui::FormDataBase *ui;
    void addItemServices(QTableWidget *widget, Service service);
    void addItemFunction(QTableWidget *widget, Function funtion);
    void addItemParameter(QTableWidget *widget, Parameter parameter);

signals:
    void sig_updateWidgetFunctions();

};

#endif // FORMDATABASE_H
