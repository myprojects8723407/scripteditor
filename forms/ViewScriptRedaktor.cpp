#include "ViewScriptRedaktor.h"
#include "DataBase.h"
#include "QDebug"

ViewScriptRedaktor::ViewScriptRedaktor()
{
    qRegisterMetaType <ScriptElement *> ("ScriptElement *");
    qRegisterMetaType <Script * > ("Script * ");
    qRegisterMetaType <Parameter> ("Parameter");

    script_selected = 0;

    scene=new QGraphicsScene(this);
    id_last = 0;
    id_script_selected = -1;
    id_element_selected  -1;
    setTransformationAnchor(AnchorUnderMouse);
    scale(qreal(3),qreal(3));

    scene->installEventFilter(this);

    //заливка фона
    QBrush brash;
    brash.setTextureImage(QImage(":/images/image/brush.png"));
    brash.setStyle(Qt::TexturePattern);
    this->setBackgroundBrush(brash);


    connect(scene,SIGNAL(sig_element_add(ScenariyElement *,bool)),SLOT(slot_element_add(ScenariyElement *,bool)),Qt::QueuedConnection);  //создание визуального элемента
    this->setScene(scene);

    connect(this, SIGNAL(sig_script_selected(int,QString,QString)),this, SLOT(slot_script_selected(int,QString,QString)), Qt::QueuedConnection);


    formEditFunction = new FormEditFunction;


    menu_getsetGlobalValue = new QMenu(this);
    /* Создаём действия для контекстного меню */
    QAction * read_value = new QAction(trUtf8("Считать значение"), this);
    QAction * write_value = new QAction(trUtf8("Установить значение"), this);
    /* Подключаем СЛОТы обработчики для действий контекстного меню */
    connect(read_value, SIGNAL(triggered()), this, SLOT(slot_readGlobalValueChange()));     // Обработчик вызова диалога редактирования
    connect(write_value, SIGNAL(triggered()), this, SLOT(slot_writeGlobalValueChange())); // Обработчик удаления записи
    /* Устанавливаем действия в меню */
    menu_getsetGlobalValue->addAction(read_value);
    menu_getsetGlobalValue->addAction(write_value);



}

void ViewScriptRedaktor::slot_script_selected(int id,QString name,QString)
{
    id_element_selected = -1;
    script_selected = DataBase::Scripts.value(name);
    if (script_selected)
    {
        set_script(script_selected);
        if (script_selected->is_start)
            emit sig_script_started(true);
        else
            emit sig_script_started(false);
    }
}

//------------------------передаем выбраный сценарий в поток отработки
void ViewScriptRedaktor::slot_start_script()
{
    script_selected->is_start = true;
    emit sig_start_script(script_selected,id_element_selected);
}

void ViewScriptRedaktor::slot_stop_script()
{
    script_selected->is_start = false;
    emit sig_stop_script(script_selected);
}


void ViewScriptRedaktor::slot_selected(int id)
{
    id_element_selected=id;
}





//------------------------ добавить выбранную функцию на сцену -----------------
void ViewScriptRedaktor::slot_funct_doubleClick(int service_id, int id_funct,QPoint point_on_scene)
{
    ScriptElement *element=new ScriptElement;
    if (DataBase::get_service_byId(service_id,element->service)){
        if (DataBase::get_function_byFunctionId(element->service.service_id,id_funct,element->function)) //получаем функцию
        {
            DataBase::get_parameters_byServiceFunctionIdentation(element->function.id, element->parameters); //получаем параметры функции

            //element->id_parent = 0;
            element->point = point_on_scene;

            slot_element_add(element,true); //рисуем и добавляем в базу

            scene->update();

        }
        else delete element;
    }
    else delete element;
    //form_tree_view->close();

}

//void ViewScenariyRedaktor::slot_set_function_text(ScenariyElement_* el)
//{
//    ScenariyElement_* element = script_selected->elemens.value(id_element);
//    element->script = text;
//}

void ViewScriptRedaktor::slot_parameter_change(ScriptElement *)
{

}

//---------------------перезагружаем скрипт с обновленными данными
void ViewScriptRedaktor::reloadScript()
{
    functionManager.resetPinSelected(); //сбрасываем выделенные элементы
    functionManager.resetFunctionSelected();
    functionManager.resetFunctionSelectedLast();
    functionManager.resetAll();
    emit sig_script_selected(script_selected->id,script_selected->name,script_selected->comment); //перезагружаем скрипт

    script_selected = 0;

}

bool ViewScriptRedaktor::eventFilter(QObject *obj, QEvent *event_)
{
    if (obj == scene){

        if (event_->type() == QEvent::Enter )
        {
            if (global_values_mouse_press==true){
                global_values_mouse_press = false;
                /* Создаем объект контекстного меню */

                menu_getsetGlobalValue->popup(QCursor::pos());
                /* Вызываем контекстное меню */
            }
        }

        QKeyEvent* event_key =dynamic_cast<QKeyEvent*>(event_);
        if (event_key){
            if (event_key->key() == Qt::Key_Delete){
                if (script_selected && functionManager.getPinSelectedLast()&&  script_selected->elemens.count()>0){ //удаляем последний выбранный соединитель
                    ScriptElement* element = script_selected->elemens.value(functionManager.getPinSelectedLast()->getFunction()->getId());

                    if (element){
                        int num = 0;
                        foreach (Parameter par, element->parameters) {
                            if (par.parameters_name == functionManager.getPinSelectedLast()->parameter.parameters_name)
                            {
                                element->parameters[num].is_delete = true;

                                //сбрасываем все соединения
                                element->parameters[num].id_element_connect = -1;
                                element->parameters[num].parameters_name_connect = "";
                                element->parameters[num].value = "";
                            }
                            num ++;
                        }

                        element->is_update = true; //при сохранении в БД информация обновиться

                        reloadScript();//перезагружаем скрипт с обновленными данными

                        return true;
                    }


                }
                else if (script_selected && functionManager.getFunctionSelectedLast() &&  script_selected->elemens.count()>0){ //удаляем функцию

                    if (script_selected->elemens.value(functionManager.getFunctionSelectedLast()->getId())->function.comand_name == "Старт") return true; //запрещаем удалять стартовый элемент

                    script_selected->elemens[functionManager.getFunctionSelectedLast()->getId()]->is_deleted = true; //помечаем для удаления
                    reloadScript(); //перезагружаем скрипт с обновленными данными
                    return true;
                }

            }
            if (event_key->key() == Qt::Key_Escape){
                functionManager.resetFunctionSelectedLast();
                functionManager.resetPinSelectedLast(); //сбрасываем выделенные элементы
                functionManager.resetAll();
                //form_tree_view->close();
                scene->update();
                return true;
            }

        }

        QGraphicsSceneMouseEvent* event =dynamic_cast<QGraphicsSceneMouseEvent*>(event_);
        if (event){

            //Ищем элемент под курсором и делаем его активным
            functionManager.FindFunctionsOrPinsByPoint(event->scenePos().toPoint());

            if (event->type() == QEvent::GraphicsSceneMousePress) {

                //if (functionManager.getFunctionActive()){
                // нажали мышку в координатах окна item
                if (event->button()==Qt::MouseButton::LeftButton){
                    mouse_left_button_press = true;
                    if (functionManager.SelectFunction(scene,event->scenePos().toPoint())){
                        if (functionManager.getFunctionSelected() && script_selected && script_selected->elemens.count()>0){
                            slot_selected(functionManager.getFunctionSelected()->getId()); //делаем текущий элемент стартовым для выполнения сценария
                            ScriptElement* element = script_selected->elemens.value(functionManager.getFunctionSelected()->getId());
                            emit sig_set_parameters_to_listWidget(element);
                        }
                        return true;
                    }

                }
                if (event->button()==Qt::MouseButton::RightButton){
                    mouse_left_button_press = false;
                    if (functionManager.getPinActive()){
                        //выделяем пин со сплайном для удаления
                        functionManager.setPinSelectedLast();
                        scene->update();
                    }
                    else{
                        emit sig_contextMenuEvent(event->screenPos(),event->scenePos().toPoint());
                    }
                    return true;
                }
                // }
            }

            if (event->type() == QEvent::GraphicsSceneMouseDoubleClick) {
                //двойной клик открывает окно редактирования скрипта функции
                if (functionManager.getFunctionActive() && script_selected && script_selected->elemens.count()>0){
                    ScriptElement* element = script_selected->elemens.value(functionManager.getFunctionActive()->getId());
                    if (element){
                        formEditFunction->setElement(element);
                        formEditFunction->show();
                        return true;
                    }
                }
            }

            if (event->type() == QEvent::GraphicsSceneMouseRelease) {

                if (mouse_left_button_press)
                {
                    mouse_left_button_press = false;
                    if (functionManager.getFunctionActive() && mouse_left_button_move && script_selected && script_selected->elemens.count()>0){
                        mouse_left_button_move = false;
                        // отпустили мышку в координатах окна item устанавливаем новые координаты элемента в БД
                        script_selected->elemens[functionManager.getFunctionActive()->getId()]->point =
                                QPoint(event->scenePos().toPoint().x()-functionManager.getFunctionActive()->delta.x(),
                                       event->scenePos().toPoint().y()-functionManager.getFunctionActive()->delta.y()); //для того чтобы убрать смещение элемента при обновлении окна

                        script_selected->elemens[functionManager.getFunctionActive()->getId()]->is_update = true;

                    }


                    G_Pin* parent = 0;
                    G_Pin* child = 0;
                    //подключаем
                    bool result = functionManager.createConnectionPinToPin(parent,child);
                    if (result && script_selected && script_selected->elemens.count()>0){ //соединение установлено
                        ScriptElement* element = script_selected->elemens.value(child->getFunction()->getId());
                        if (element){
                            int num=0;
                            foreach (Parameter var, element->parameters) {
                                if (var.parameters_name == child->getPinName()) //находим нужный пин по имени переменной
                                {
                                    element->parameters[num].id_element_connect = parent->getFunction()->getId();
                                    element->parameters[num].parameters_name_connect = parent->getPinName();
                                    element->is_update = true;
                                    break;
                                }
                                num++;
                            }

                        }



                    }

                    functionManager.resetPinSelected(); //сбрасываем выделенные элементы
                    functionManager.resetFunctionSelected();
                }

                this->setCursor(QCursor((Qt::ArrowCursor)));
                scene->update();

                return true;
            }

            if (event->type() == QEvent::GraphicsSceneMouseMove) {

                if (!functionManager.getPinActive() && functionManager.getFunctionActive())
                    this->setCursor(QCursor(Qt::ClosedHandCursor));  //меняем курсор на руку
                else if (functionManager.getPinActive() || functionManager.getPinSelected())
                    this->setCursor(QCursor(Qt::CrossCursor)); //меняем курсор на крест
                else
                    this->setCursor(QCursor((Qt::ArrowCursor)));//стандартный курсор


                if (mouse_left_button_press){
                    if (!functionManager.getPinSelected() && functionManager.getFunctionSelected()){
                        mouse_left_button_move = true;
                        //перемещаем прямоугольник со сплайнами
                        functionManager.setActiveFunctionPosition(event->scenePos().toPoint());
                        scene->update();
                        return true;

                    }


                    //if (  functionManager.getPinActive() || functionManager.getPinSelected()){
                    //отслеживание соединение пинов сплайном
                    if (functionManager.tryPinsConnect(event->scenePos().toPoint()))
                    {

                        scene->update();
                    }

                    return true;

                    //  }
                }

            }

        }


    }
    else {
        // standard event processing
        return QObject::eventFilter(obj, event_);
    }


}


void ViewScriptRedaktor::wheelEvent(QWheelEvent *event)
{

    const QPointF p0scene = mapToScene(event->pos());



    delta_zoom = delta_zoom + event->delta()/4.0;

    if (delta_zoom<-250) {
        delta_zoom = -250;
        return;
    }
    if (delta_zoom>0) {
        delta_zoom = 0;
        return;
    }



    qreal factor = std::pow(1.01, event->delta()/4.0);

    scale(factor, factor);

    const QPointF p1mouse = mapFromScene(p0scene);
    const QPointF move = p1mouse - event->pos(); // The move
    horizontalScrollBar()->setValue(move.x() + horizontalScrollBar()->value());
    verticalScrollBar()->setValue(move.y() + verticalScrollBar()->value());
    //    QRect rect;
    //    rect.setWidth(horizontalScrollBar()->maximum()-horizontalScrollBar()->minimum());
    //    rect.setHeight(verticalScrollBar()->maximum() - verticalScrollBar()->minimum());
    //    scene->setSceneRect(rect);

}


//сценарий выбран для редактирования
void ViewScriptRedaktor::set_script(Script *script)
{
    if (!script) return;

    int id = DataBase::getScriptIdByName(script);
    if (id==-1) return;


    script->id = id;


    id_script_selected=id;
    script_selected=script;

    scene->clear();

    int maxX=-100000;
    int minX=100000;
    int maxY=-100000;
    int minY=100000;



    if (script->elemens.count()==0) //если пустой скрипт то добавляем стартовый элемент
    {
        ScriptElement *element=new ScriptElement;
        element->point = QPoint(0,0);
        DataBase::get_service_byIdentation(DataBase::service_index_function_c,element->service);
        QList <Function> functions;
        DataBase::get_functions_byServiceId(element->service.service_id,functions);
        foreach (Function var, functions) {
            if (var.comand_name == "Старт")
            {
                element->function = var;
                Parameter par;
                par.inout_type = 1;
                par.parameters_name = "Выход";
                par.parameters_type = "connector_out";
                par.parameter_type = "default";
                element->parameters.append(par);
            }
        }
        slot_element_add(element,true); //рисуем и добавляем в базу
    }


    functionManager.resetAll();

    QList <int> keys=script->elemens.keys();
    for (int i=0; i<keys.count(); i++)
    {
        ScriptElement *el=script->elemens.value(keys.value(i));
        el->id_script=script->id;

        if (el->is_deleted == true)
            continue;

        QPoint point(el->point.x(),el->point.y());
        slot_element_add(el,false);

        if (maxX<el->point.x())
            maxX=el->point.x();
        if (minX>el->point.x())
            minX=el->point.x();
        if (maxY<el->point.y())
            maxY=el->point.y();
        if (minY>el->point.y())
            minY=el->point.y();

    }


    for (int i=0; i<keys.count(); i++)
    {
        ScriptElement *el=script->elemens.value(keys.value(i));

        if (el->is_deleted == true)
            continue;

        foreach (Parameter par, el->parameters) {
            if (par.id_element_connect!=-1 /*&& !par.is_delete*/){ //пин подключен и не помечен для удаления
                int id_parent = par.id_element_connect;
                int id_child = el->id;
                G_Function *funct_parent = functionManager.getFunctionById(id_parent);
                G_Function *funct_child = functionManager.getFunctionById(id_child);
                if (funct_parent && funct_child){
                    G_Pin *pin_in = funct_child->getPinsByNum(par.parameters_name);
                    G_Pin *pin_out = funct_parent->getPinsByNum(par.parameters_name_connect);
                    if (pin_in && pin_out){
                        G_LineConnector *line=new G_LineConnector();
                        if (!par.parameters_type.contains("connector") && funct_parent->getFunction().comand_name != "Установить") //для коннекторов меняем концы подключений так как они считаются по ходу сценария а переменные читаются из родительского элемента
                            line->set_pos(funct_parent->mapToScene(pin_out->getPoint()),funct_child->mapToScene(pin_in->getPoint()));
                        else
                            line->set_pos(funct_child->mapToScene(pin_in->getPoint()),funct_parent->mapToScene(pin_out->getPoint()));

                        pin_in->appendLineConnector(line);
                        pin_out->appendLineConnector(line);
                        //line->addConnector(pin_in);
                        //line->addConnector(pin_out);
                        scene->addItem(line);

                    }

                }
            }
        }
    }



    //scene->setSceneRect(minX,minY,maxX,maxY);
    //scene->setSceneRect(scene->itemsBoundingRect());
    scene->update();
}



void ViewScriptRedaktor::slot_timer_rabota()
{
    //    if (!DM.flag_emul){
    //        date_time_tek=QDateTime::currentDateTime();
    //        //slot_draw(date_time_tek);
    //    }
    //    else {
    //        DM.date_time_emul=DM.date_time_emul.addMSecs(DM.delta_msec_emul);
    //        //slot_draw(DM.date_time_emul);
    //    }


}

//--------меняем цвет прямоугольника в зависисмоти от результатов работы функций
void ViewScriptRedaktor::slot_state(Script* script,int state, QString error,int id)
{
    if (script == script_selected){
        if (state == StateFunction::Finish) //скрипт завершил работу
        {
            if (script_selected == script && script->is_start) //если скрипт загружен в окне
                emit sig_script_started(false); //шлем сигнал на отжатие кнопки запуска
            script->is_start = false;
        }
        for (int i=0; i<items().count();i++){
            G_Function *item=dynamic_cast<G_Function*> (items().value(i));
            //G_Function *element=dynamic_cast<G_Function*> (id);
            if (item){
                if (item->getId()==id){
                    if (state==StateFunction::Process) //функция на исполнении
                        item->setColor(Qt::yellow);
                    if (state==StateFunction::EndOk)//функция завершена
                        item->setColor(Qt::green);
                    if(state==StateFunction::EndError)
                    {

                        /*ErrResultOk         = 0,
                                        ErrResultError      = 1,
                                        ErrCrcError         = 2,
                                        ErrNotInit          = 3,
                                        ErrParamsDecode     = 4,
                                        ErrParamsValue      = 5,
                                        ErrExtra            = 6,
                                        ErrTimeout          = 7,
                                        ErrAbort            = 8,
                                        ErrMemAlloc         = 9,
                                        ErrSockError        = 10,
                                        ErrNotFound         = 11,
                                        ErrNotAllowed       = 12,
                                        ErrFrameIncomplete  = 13,
                                        ErrEquipTrash       = 14,
                                        ErrExists           = 15,
                                        ErrDataEncode       = 16,
                                        ErrEquipBusy            = 17,
                                        ErrEquipConnectionFail  = 18,
                                        ErrDenied           = 19,
                                        ErrConfirmTimeout   = 20,
                                        ErrCanceled         = 21,
                                        ErrServiceConnection = 22,
                                        ErrDate              = 23*/


                        item->setError(error);

                        item->setColor(Qt::red);

                        for (int j=i+1; j<items().count();j++){
                            G_Function *item2=dynamic_cast<G_Function*> (items().value(i));
                            item2->setError("Пропущена");
                            item2->setColor(Qt::red);
                        }
                    }
                }


                scene->update();
            }
        }
    }



}

//--------------- добавление глобальной переменной
void ViewScriptRedaktor::GlobalValueElementAdd( int type)
{
    if (!nameGlobalValue.isEmpty())
    {
        ScriptElement *element=new ScriptElement;

        //element->id_parent = 0;

        DataBase::get_service_byIdentation(DataBase::service_index_function_c ,element->service);


        if (type == 1)
            DataBase::get_function_byName(element->service.service_id ,"Получить",element->function);
        if (type == 0)
            DataBase::get_function_byName(element->service.service_id ,"Установить",element->function);

        Parameter par;
        par.parameters_name = nameGlobalValue;
        nameGlobalValue = "";
        par.inout_type = type;
        element->parameters.append(par);
        QPoint viewPoint =this->mapFromGlobal(QCursor::pos());
        QPointF scenePoint = this->mapToScene(viewPoint);
        element->point = scenePoint.toPoint();

        slot_element_add(element,true); //рисуем и добавляем в базу
        menu_getsetGlobalValue->close();

        scene->update();
    }
}

void ViewScriptRedaktor::slot_readGlobalValueChange()
{

    GlobalValueElementAdd(1);
}

void ViewScriptRedaktor::slot_writeGlobalValueChange()
{
    GlobalValueElementAdd(0);
}



void  ViewScriptRedaktor::slot_element_set_pos(int id,QPoint point)  //изменяем позицию элемента в базе при перетаскивании элемента
{
    emit sig_element_set_pos(id, point);
}

void  ViewScriptRedaktor::slot_element_set_id(int id,QPoint point)  //установка id после записи в базу
{
    for (int i=0; i<items().count();i++){
        G_Function *item=dynamic_cast<G_Function*> (items().value(i));
        if (item->x()==point.x() && item->y()==point.y()){
            item->setId(id);
        }

    }
}

//--------------------------------------создание или отрисовка  элемента--------------------------------------
void ViewScriptRedaktor::slot_element_add(ScriptElement * el,bool flag_to_BD)
{
    if (!script_selected)
        return;

    if (flag_to_BD){ //запись в базу
        id_last++; //ели это новый элемент то добавляем счетчик id
        el->id_script=script_selected->id;
        el->id=id_last;
        el->is_new = true; //новый элемент
        el->is_update = false;
        el->is_deleted = false;
        //emit sig_element_add_bd(el); //сохраняем в БД
        script_selected->elemens.insert(el->id,el);
    }


    if (el->service.service_identation>-1){

        G_Function *g_function = new G_Function(el->id);
        g_function->setService(el->service);

        if (el->service.service_identation >= DataBase::service_index_function_c){
            if (el->function.comand_name == "Получить" || el->function.comand_name == "Установить")
            {
                el->function.color_head =  QColor(200,200,0);
                //удаляем лишние входы и выходы
                int num = 0;
                foreach (Parameter par, el->parameters) {
                    if (par.parameters_type.contains("connector"))
                        el->parameters.removeAt(num--);
                    num++;
                }
            }
            else el->function.color_head = QColor(0,100,0);


            if (el->function.comand_name == "Старт")
                //удаляем выход у стартового элемента
            {
                int num = 0;
                foreach (Parameter par, el->parameters) {
                    if (par.parameters_type.contains("connector") && par.parameters_name == "Вход")
                        el->parameters.removeAt(num--);
                    num++;
                }
            }



        }
        else {
            el->function.color_head = Qt::blue;
        }



        g_function->setFunction(el->function);
        g_function->setParameters(el->parameters);
        g_function->setPos(el->point);


        scene->addItem(g_function); //добавляем на сцену
        functionManager.appendFunction(g_function);


    }

    scene->update();


}



//------------- Установка номера последнего id считанного из базы ----------------
void ViewScriptRedaktor::setIdLast(int id)
{
    id_last=id;
}

//-------------- поставить цвет по умолчанию
void ViewScriptRedaktor::resetFunctions()
{
    for (int i=0; i<items().count();i++){
        G_Function *item=dynamic_cast<G_Function*> (items().value(i));
        //G_Function *element=dynamic_cast<G_Function*> (id);
        if (item){
            item->setError(false);
            item->setColor(Qt::black);

        }
    }

    scene->update();

    if (script_selected){
        for (int i=0; i<script_selected->elemens.count();i++){
            ScriptElement *element=script_selected->elemens.value(i);

            if (element){
                int num = 0;
                foreach (Parameter parameter, element->parameters) {
                    element->script_error = "";
                    if (parameter.inout_type == 1){
                        element->parameters[num].value = "";
                    }
                    num++;
                }
            }
        }
    }
}


























