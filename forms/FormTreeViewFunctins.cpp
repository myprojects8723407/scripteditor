#include "FormTreeViewFunctions.h"
#include "ui_FormTreeViewFunctins.h"

FormTreeViewFunctins::FormTreeViewFunctins(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FormTreeViewFunctins)
{
    ui->setupUi(this);

    model=new QStandardItemModel;
    ui->treeView->setModel(model);
    QStringList labels;
    labels<<"Добавить функцию в сценарий";
    model->setHorizontalHeaderLabels(labels);
    connect(ui->treeView,SIGNAL(doubleClicked(const QModelIndex &)),SLOT(doubleClicked(const QModelIndex &)));
    connect(this,SIGNAL(),SLOT(doubleClicked(const QModelIndex &)));

}

FormTreeViewFunctins::~FormTreeViewFunctins()
{
    delete ui;
}

void FormTreeViewFunctins::appendService(int id_service, QString name_service)
{

    QList <QStandardItem*> items=model->findItems(name_service);
    if (items.count()==0){
        QStandardItem *item=new QStandardItem;
        item->setData(id_service,Qt::UserRole+1);
        item->setData(0,Qt::UserRole+2); //
        item->setData(name_service,Qt::DisplayRole);
        item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
        model->appendRow(item);
    }


}

void FormTreeViewFunctins::appendFunction(int id_funct,  QString name_service, QString name_funct)
{
    if (name_service == "Управление выполнением сценария" && (name_funct=="Установить" || name_funct=="Получить"))
        return;

    QList <QStandardItem*> items=model->findItems(name_service);
    if (items.count()>0){
        QStandardItem *item=new QStandardItem;
        item->setData(id_funct,Qt::UserRole+1);
        item->setData(1,Qt::UserRole+2); //
        item->setData(name_funct,Qt::DisplayRole);
        item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
        items[0]->appendRow(item);
    }
}

void FormTreeViewFunctins::doubleClicked(const QModelIndex &index)
{
   int isRoot=model->data(index,Qt::UserRole+2).toInt();
   if (isRoot!=0){
        int id_service=model->data(index.parent(),Qt::UserRole+1).toInt();
        int id_funct=model->data(index,Qt::UserRole+1).toInt();
        emit sig_funct_doubleClick(id_service,id_funct,point_on_scene);
        this->close();
   }

}

void FormTreeViewFunctins::hideEvent(QHideEvent *event)
{
    this->close();
}

void FormTreeViewFunctins::focusOutEvent(QFocusEvent *event)
{
    this->close();
}

void FormTreeViewFunctins::leaveEvent(QEvent *event)
{
     this->close();
}
