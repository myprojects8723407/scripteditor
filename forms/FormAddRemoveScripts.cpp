#include "FormAddRemoveScripts.h"
#include "DataBase.h"

FormScripts::FormScripts()
{
    this->setFixedWidth(300);
    connect(this,SIGNAL(clicked(QModelIndex)),SLOT(slot_select_script(QModelIndex)),Qt::QueuedConnection);

}
//при клике мышкой по таблице
void FormScripts::slot_select_script(QModelIndex index)
{

    int count= this->model->rowCount();
    int script_id=this->model->itemData(this->model->index(index.row(),0)).value(0).toInt();
    QString script_name=this->model->itemData(this->model->index(index.row(),1)).value(0).toString();
    QString script_comment=this->model->itemData(this->model->index(index.row(),2)).value(0).toString();

    emit sig_script_selected(script_id,script_name,script_comment);


}



//добавить сценарий
void FormScripts::slot_append_script(QString script_name,QString komment)
{
    if (script_name!=""){
        if (model){
            int lastRow = this->model->rowCount();
            this->model->insertRow(lastRow);
            this->model->setData(this->model->index(lastRow,1),script_name);
            this->model->setData(this->model->index(lastRow,2),komment);
            this->model->submitAll();
            this->model->select();
            this->selectRow(lastRow);
            this->setFocus();
            slot_select_script(this->model->index(lastRow,1));
        }
        else {
            QMessageBox msg;
            msg.setWindowTitle("Внимание");
            msg.setText("Отсутствует подключение к БД");
            msg.exec();
        }
    }
    else {
        QMessageBox msg;
        msg.setWindowTitle("Внимание");
        msg.setText("Введите название сценария");
        msg.exec();
    }
}


//удалить сценарий
void FormScripts::slot_remove_script()
{
    QMessageBox msgBox;
    msgBox.setText("Внимание!");
    msgBox.setInformativeText("Вы действительно хотите удалить выбранный сценарий?");
    msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Cancel);
    int ret = msgBox.exec();
    if (ret==QMessageBox::Ok){
        int row=this->currentIndex().row();
        QString name = this->model->data(this->model->index(this->currentIndex().row(),1)).toString();
        DataBase::removeScript(name);
        this->model->removeRow(row);
        this->model->submitAll();
        this->model->select();
        this->selectRow(row);
        this->setFocus();
    }

}
