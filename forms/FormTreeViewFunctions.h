#ifndef FORMTREEVIEWFUNCTINS_H
#define FORMTREEVIEWFUNCTINS_H

#include <QWidget>
#include <QStringListModel>
#include <QStandardItemModel>
#include <QStandardItem>
namespace Ui {
class FormTreeViewFunctins;
}

class FormTreeViewFunctins : public QWidget
{
    Q_OBJECT

public:
    explicit FormTreeViewFunctins(QWidget *parent = 0);
    ~FormTreeViewFunctins();

    QPoint point_on_scene; //координаты на сцене

    void appendFunction(int id_funct, QString name_service, QString name_funct);
    void appendService(int id_service, QString name_service);
private:
    Ui::FormTreeViewFunctins *ui;

    QStandardItemModel *model;

private slots:
   void  doubleClicked(const QModelIndex &index);

signals:
   void sig_funct_doubleClick(int id_service,int id_funct,QPoint);

protected:

   virtual void  hideEvent(QHideEvent *event) override;

   virtual void   focusOutEvent(QFocusEvent *event) override;

   virtual void    leaveEvent(QEvent *event) override;



};

#endif // FORMTREEVIEWFUNCTINS_H
