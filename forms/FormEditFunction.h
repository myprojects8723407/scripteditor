#ifndef FORMEDITFUNCTION_H
#define FORMEDITFUNCTION_H

#include <QWidget>
#include <QComboBox>
#include <QLineEdit>
#include <QTableWidgetItem>
#include "Structures.h"
#include "models/MyComboDelegate.h"


class DataBase;

namespace Ui {
class FormEditFunction;
}

class FormEditFunction : public QWidget
{
    Q_OBJECT

public:
    explicit FormEditFunction(QWidget *parent = 0);
    ~FormEditFunction();

    void setElement(ScriptElement*);

public slots:

    void slot_newFunction();
private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();
    void on_pushButton_append_parameter_in_clicked();
    void on_pushButton_delete_parameter_in_clicked();
    void on_pushButton_append_parameter_out_clicked();
    void on_pushButton_delete_parameter_out_clicked();
    void on_pushButton_append_parameter_connectors_out_clicked();
    void on_pushButton_delete_parameter_connectors_out_clicked();
signals:
    void sig_save_function_to_bd(ScriptElement*);
    void sig_change_function_element(ScriptElement*);
    void sig_ParameterChange(int);

private:
    Ui::FormEditFunction *ui;

    ScriptElement* element = 0;
    int id_element = 0;
    void AppendParameter(QTableWidget *table, Parameter parameter, QString name_in_function);
    void RemoveParameter(QTableWidget *table);

    void appendParameters(QTableWidget *table);
protected:

protected slots:

};

#endif // FORMEDITFUNCTION_H
