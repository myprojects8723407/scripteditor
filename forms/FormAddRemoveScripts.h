#ifndef FORMSCENARIES_H
#define FORMSCENARIES_H

#include <QWidget>
#include <QDialog>
#include <QSqlRecord>
#include <QMessageBox>
#include <models/TableModelScripts.h>
#include <models/TableModelScripts.h>
#include "Structures.h"

class DataBase;

class FormScripts : public TableModelScritps
{
    Q_OBJECT
public:
    explicit FormScripts();
    //TableModelScritps *model_scripts;
    QModelIndex index_selected; //выбранный индекс сценария

public slots:

    void slot_select_script(QModelIndex); //при клике мышкой по  таблице сценария
    void slot_remove_script();
    void slot_append_script(QString script_name,QString komment); //добавить название сценария


signals:
   void sig_script_selected(int script_id,QString script_name,QString script_comment);


};

#endif // FORMSCENARIES_H
