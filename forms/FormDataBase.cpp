#include "FormDataBase.h"
#include "ui_FormDataBase.h"
#include "DataBase.h"

FormDataBase::FormDataBase(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FormDataBase)
{
    ui->setupUi(this);
    connect(ui->tableWidget_services,SIGNAL(cellClicked(int,int)),SLOT(slot_services_cellClicked(int,int)));
    connect(ui->tableWidget_functions,SIGNAL(cellClicked(int,int)),SLOT(slot_functions_cellClicked(int,int)));

    ui->tableWidget_services->setColumnCount(3);
    ui->tableWidget_services->setColumnWidth(0,50);
    ui->tableWidget_services->setColumnWidth(1,100);
    ui->tableWidget_services->setColumnWidth(2,500);
    QStringList horzHeaders;
    horzHeaders << "ИД" << "Тип";
    ui->tableWidget_services->setHorizontalHeaderLabels( horzHeaders );


    QTableWidgetItem *item = new QTableWidgetItem;
    item->setText("Наименование");
    item->setTextAlignment(Qt::AlignLeft);
    ui->tableWidget_services->setHorizontalHeaderItem(2,item);


    ui->tableWidget_functions->setColumnCount(2);
    ui->tableWidget_functions->setColumnWidth(1,500);
    horzHeaders.clear();
    horzHeaders <<  "Имя"  ;
    ui->tableWidget_functions->setHorizontalHeaderLabels( horzHeaders );

    item = new QTableWidgetItem;
    item->setText("Описание");
    item->setTextAlignment(Qt::AlignLeft);
    ui->tableWidget_functions->setHorizontalHeaderItem(1,item);

    ui->tableWidget_parameters->setColumnCount(2);
    ui->tableWidget_parameters->setColumnWidth(1,500);
    horzHeaders.clear();
    horzHeaders <<  "Тип" ;
    ui->tableWidget_parameters->setHorizontalHeaderLabels( horzHeaders );

    item = new QTableWidgetItem;
    item->setText("Наименование");
    item->setTextAlignment(Qt::AlignLeft);
    ui->tableWidget_parameters->setHorizontalHeaderItem(1,item);
}

FormDataBase::~FormDataBase()
{
    delete ui;
}

void FormDataBase::addItemServices(QTableWidget *table, Service service)
{
    table->insertRow(table->rowCount());

    QTableWidgetItem* taskItem = new QTableWidgetItem();
    taskItem->setText(QString::number(service.service_identation));
    table->setItem(table->rowCount()-1,0,taskItem);

    taskItem = new QTableWidgetItem();
    taskItem->setText(service.service_name);
    table->setItem(table->rowCount()-1,2,taskItem);

    taskItem = new QTableWidgetItem();
    taskItem->setText(service.service_system_name);
    table->setItem(table->rowCount()-1,1,taskItem);


    taskItem = new QTableWidgetItem();
    taskItem->setText(QString::number(service.service_id));
    table->setVerticalHeaderItem(table->rowCount()-1,taskItem);
}

void FormDataBase::addItemFunction(QTableWidget *table, Function funtion)
{
    table->insertRow(table->rowCount());

    QTableWidgetItem* taskItem = new QTableWidgetItem();
    taskItem->setText(funtion.comand_name);
    table->setItem(table->rowCount()-1,0,taskItem);

    taskItem = new QTableWidgetItem();
    taskItem->setText(funtion.comand_about);
    table->setItem(table->rowCount()-1,1,taskItem);


    taskItem = new QTableWidgetItem();
    taskItem->setText(QString::number(funtion.id));
    table->setVerticalHeaderItem(table->rowCount()-1,taskItem);
}

void FormDataBase::addItemParameter(QTableWidget *table, Parameter parameter)
{
    table->insertRow(table->rowCount());

    QTableWidgetItem* taskItem = new QTableWidgetItem();
    taskItem->setText(parameter.parameters_name);
    table->setItem(table->rowCount()-1,1,taskItem);

    taskItem = new QTableWidgetItem();
    taskItem->setText(parameter.parameters_type);
    table->setItem(table->rowCount()-1,0,taskItem);

    taskItem = new QTableWidgetItem();
    taskItem->setText(QString::number(parameter.id));
    table->setVerticalHeaderItem(table->rowCount()-1,taskItem);
}

void FormDataBase::on_pushButton_delete_service_clicked()
{
    int row = ui->tableWidget_services->currentRow();
    int service_id = ui->tableWidget_services->verticalHeaderItem(row)->text().toInt();
    DataBase::removeService(service_id);
    ChangeData();
}

void FormDataBase::on_pushButton_delete_function_clicked()
{
    int row = ui->tableWidget_functions->currentRow();
    int function_id = ui->tableWidget_functions->verticalHeaderItem(row)->text().toInt();
    DataBase::removeFunction(function_id);
    ChangeData();
}

void FormDataBase::on_pushButton_delete_parameter_clicked()
{
    int row = ui->tableWidget_parameters->currentRow();
    int parameter_id = ui->tableWidget_parameters->verticalHeaderItem(row)->text().toInt();
    DataBase::removeParameter(parameter_id);
    ChangeData();
}

void FormDataBase::slot_services_cellClicked(int row, int)
{
    ui->tableWidget_functions->setRowCount(0);
    ui->tableWidget_parameters->setRowCount(0);
    int service_id = ui->tableWidget_services->verticalHeaderItem(row)->text().toInt();
    QList <Function> functions;
    if (DataBase::get_functions_byServiceId(service_id,functions)){
        foreach (Function function, functions) {
            addItemFunction(ui->tableWidget_functions,  function);
        }
    }
    slot_functions_cellClicked(0,0);
}

void FormDataBase::slot_functions_cellClicked(int row, int)
{
    ui->tableWidget_parameters->setRowCount(0);
    if (ui->tableWidget_functions->rowCount()>row){
        int function_id = ui->tableWidget_functions->verticalHeaderItem(row)->text().toInt();
        QList <Parameter> parameters;
        if (DataBase::get_parameters_byServiceFunctionIdentation(function_id, parameters,false)){
            foreach (Parameter parameter, parameters) {
                addItemParameter(ui->tableWidget_parameters,  parameter);
            }
        }
    }
}

void FormDataBase::ChangeData()
{
    ui->tableWidget_services->setRowCount(0);
    ui->tableWidget_functions->setRowCount(0);
    ui->tableWidget_parameters->setRowCount(0);

    QList <Service> services;
    if (DataBase::get_services(services)){
        foreach (Service service, services) {

            addItemServices(ui->tableWidget_services,  service);

        }
    }

    ui->tableWidget_services->setCurrentCell(0,0);
    slot_services_cellClicked(0,0);
    slot_functions_cellClicked(0,0);
    emit sig_updateWidgetFunctions();
}

void FormDataBase::showEvent(QShowEvent *event)
{

    ChangeData();

}
