#ifndef FORMREDACTOR_H
#define FORMREDACTOR_H

#include <QMainWindow>
#include <QSqlRecord>
#include <QMessageBox>
#include <models/TableModelScripts.h>
#include <forms/ViewScriptRedaktor.h>
#include "Structures.h"
#include "FormAddRemoveScripts.h"
#include "FormAddScenariy.h"
#include "tableWidgetParamreters.h"
#include "FormDataBase.h"

#include <models/TableModelScripts.h>


class GlobalValues;

class FormScriptRedactor : public QMainWindow
{
    Q_OBJECT
    void add_global_value(Parameter* value);
    void changeGlobalValue(int num);
    void changeGlobalValues();
    FormDataBase* formdatabase;
    FormAddScenariy *form_add_script;
    ListWidgetParamreters *listWidgetParamreters;
    QAction *act_script_change ;
    QAction *act_script_start_stop ;
    QAction *act_add_function;
    QAction *reload_services;
    QTableWidget *global_value;

    FormTreeViewFunctins *form_tree_view; //форма с перечислением функций показвающаяся при нажатии на правую кнопку



    QIcon start_icon;
    QIcon stop_icon;

    void updateWidgetFunctions();
public:
    explicit FormScriptRedactor(QWidget *parent = nullptr);

    ViewScriptRedaktor *view_script_redactor;
    TableModelScritps *model_scenariy;
    FormScripts *formScripts;


signals:
    void sig_start_script();
    void sig_stop_script();
    void sig_append_script();
    void sig_remove_script();
    void sig_save();


public slots:
    void slot_set_id_last(int);
    void slot_script_read_from_bd(Script*);
    void slot_start_script(bool);
    void slot_reload_current_script();
    void slot_add_value();
    void slot_remove_value();
    void slot_append_global_values_to_form();
    void slot_currentItemChanged(QTableWidgetItem*,QTableWidgetItem*);
    void slot_bd_script_loaded();
    void slot_updateWidgetFunctions();


    void slot_update_values_to_form();
    void slot_change_state_button_start_script(bool flag);
    void slot_contextMenuEvent(QPoint,QPoint);
protected:
    bool eventFilter(QObject *obj, QEvent *event_);


};

#endif // FORMREDACTOR_H
