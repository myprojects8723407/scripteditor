#ifndef VIEWSCENARIYREDAKTOR_H
#define VIEWSCENARIYREDAKTOR_H

#include <QObject>
#include <QGraphicsView>
#include <QGridLayout>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QPainterPath>
#include <QListWidget>
#include <QApplication>
#include <QMenu>
#include <QToolBar>
#include<QScrollBar>
#include<QSpinBox>
#include<QGroupBox>
#include<QCheckBox>
#include<QSettings>
#include<QSlider>
#include <QSlider>
#include <QPolygonF>
#include <QAction>
#include <QBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QCoreApplication>
#include <qmath.h>
#include <QTimer>
#include <QLabel>
#include <QWheelEvent>
#include <QGraphicsSceneMouseEvent>
#include <QDateTime>
#include <QComboBox>




#include "Structures.h"
#include "graphicsItems/G_Function.h"
#include "graphicsItems/G_FunctionsManager.h"
#include "forms/FormEditFunction.h"
#include "forms/FormTreeViewFunctions.h"

class DataBase;



class ViewScriptRedaktor: public QGraphicsView
{
    Q_OBJECT
public:
    ViewScriptRedaktor();
    void set_script(Script *script);
    void setIdLast(int id);
    void resetFunctions();
    void updateWidgetFunctions(); //обновить список функций контекстного окна

    QGraphicsScene *scene;
    int sizeX;	// ширина текущего изображения
    int sizeY;//высота текущего изображения
    int X_mouse_info;
    int Y_mouse_info;
    int val_mouse_info;
    bool flag_mouse_L_press;
    bool flag_mouse_relize;
    double scale_old;
    bool resize_image;
    QDateTime date_time_tek;
    QDateTime date_time_tek_emul;
    QDateTime datetime_start;
    //QList <GRect*> items;
    QList <G_Function*> items_rect_function; //список элементов сценария
    int id_script_selected; //текущий идентификатор выбранного сценария на редактрирование
    int id_last; //последний ID в базе для последующей нумерации

    Script *script_selected;
    int id_element_selected;//выделенный элемент сценария


    QMap <int , G_LineConnector*> lines_connectors;  //функции в текущем сценарии
    G_FunctionsManager functionManager;
    FormEditFunction *formEditFunction;

    bool global_values_mouse_press = false;
    QString nameGlobalValue;


signals:
    void sig_mouse_L_press(int, int);
    void sig_mouse_R_press(int, int);
    void signal_mouse_wheel_change(int delta);
    void signal_mouse_pos_change(int X_, int Y_,int key);
    void signal_resize(bool);
    void sig_element_add(ScriptElement *,bool);
    void sig_element_add_bd(ScriptElement *);
    void sig_pin_connector_add(ScriptElement *,int num); //изменение соединения пина
    void sig_element_change(int id,int id_parent,QPointF point,int type,QString name,QString comment);  //изменяем элемент в базе
    void sig_element_set_pos(int id,QPoint point);  //изменяем позицию элемента в базе
    void sig_element_change(ScriptElement *);
    void sig_start_script(Script *script_selected,int);
    void sig_stop_script(Script *script_selected);
    void sig_reload_current_script();
    void sig_remove_pin(int,QString);
    void sig_set_parameters_to_listWidget(ScriptElement *);
    void sig_script_read_from_bd(Script*);
    void sig_script_selected(int,QString,QString);
    void sig_script_started(bool); //
    void sig_contextMenuEvent(QPoint,QPoint);



protected:
    bool eventFilter(QObject *obj, QEvent *event);
private:
    void wheelEvent(QWheelEvent *event);


public slots:
    void slot_timer_rabota();
    //void slot_state(int,QString,int);
    void slot_element_add(ScriptElement *el,bool flag_to_BD); //создание графического элемента по координатам x,y и тип элемента
    void slot_element_set_pos(int id,QPoint point);  //изменяем позицию элемента в базе
    void slot_element_set_id(int id,QPoint point);  //установка id после записи в базу

    void slot_start_script();
    void slot_selected(int id);

    void slot_funct_doubleClick(int,int,QPoint point_on_scene); //добавить функцию на сцену
    //void slot_set_function_text(int,QString);// меняем текст скрипта
    void slot_parameter_change(ScriptElement*); //изменилось значение элемента


    void slot_script_selected(int id, QString name, QString);
    void slot_stop_script();
    void slot_state(Script *script, int state, QString error_name, int id);

    void slot_readGlobalValueChange();
    void slot_writeGlobalValueChange();

private slots:


private:



    double delta_zoom = 0;
    bool mouse_left_button_press=false;
    bool mouse_left_button_move = false; //нажали мышь и переместили функцию
    int type_connecter_activate = -1;

    QMenu * menu_getsetGlobalValue;




    void reloadScript();
    void GlobalValueElementAdd(int type);
};



#endif // VIEWSCENARIYREDAKTOR_H
