#include "FormEditFunction.h"
#include "ui_FormEditFunction.h"
#include "DataBase.h"

FormEditFunction::FormEditFunction(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FormEditFunction)
{
    ui->setupUi(this);


    ui->tableWidget_parameters_in->setColumnCount(4);
    QStringList horzHeaders;
    horzHeaders << "Имя" << "Тип" ;
    ui->tableWidget_parameters_in->setHorizontalHeaderLabels( horzHeaders );

    ui->tableWidget_parameters_out->setColumnCount(4);
    horzHeaders.clear();
    horzHeaders << "Имя" << "Тип" ;
    ui->tableWidget_parameters_out->setHorizontalHeaderLabels( horzHeaders );

    ui->tableWidget_outers->setColumnCount(4);
    horzHeaders.clear();
    horzHeaders << "Имя";
    ui->tableWidget_outers->setHorizontalHeaderLabels( horzHeaders );
    ui->tableWidget_outers->setColumnHidden(1,true);
    ui->tableWidget_outers->setColumnHidden(2,true);
    ui->tableWidget_outers->setColumnHidden(3,true);




}

FormEditFunction::~FormEditFunction()
{
    delete ui;
}




//-------------- добавление параметра
void FormEditFunction::AppendParameter(QTableWidget *table,Parameter parameter,QString name_in_function)
{
    table->insertRow(table->rowCount());

    QTableWidgetItem* taskItem = new QTableWidgetItem();
    taskItem->setText(parameter.parameters_name);
    table->setItem(table->rowCount()-1,0,taskItem);

    taskItem = new QTableWidgetItem();
    taskItem->setText(parameter.parameters_type);
    table->setItem(table->rowCount()-1,1,taskItem);
    table->setItemDelegateForColumn(1,new MyComboDelegate("parameter_types","id","type"));

    taskItem = new QTableWidgetItem();
    taskItem->setText(parameter.parameter_type);
    table->setItem(table->rowCount()-1,2,taskItem);
    table->setItemDelegateForColumn(2,new MyComboDelegate("value_types","id","type"));

    taskItem = new QTableWidgetItem();
    taskItem->setText(parameter.parameter_name);
    table->setItem(table->rowCount()-1,3,taskItem);


    taskItem = new QTableWidgetItem();
    taskItem->setText(name_in_function);
    table->setVerticalHeaderItem(table->rowCount()-1,taskItem);


}
//-------------добавление новой функции в БД
void FormEditFunction::slot_newFunction()
{

    ScriptElement*   element_new = new ScriptElement;
    element_new->function.id=-1;
    element_new->function.comand_function_id=-1;
    element_new->function.comand_is_deleted=0;
   // element_new->function.service_index=43;

    setElement(element_new);

    this->show();
}
//-------------Установка данных на форму, если нажимаем мышкой на уже созданном элементе сценария то все изменения идут в элемент скрипта
void FormEditFunction::setElement(ScriptElement* el)
{
    element = el;

     ui->comboBox_groupe->clear();
    QList <Service> services;
    if (DataBase::get_services(services)){
        foreach (Service service, services) {
            ui->comboBox_groupe->addItem(service.service_name);
        }
    }


    ui->tableWidget_parameters_in->setRowCount(0);
    ui->tableWidget_parameters_out->setRowCount(0);
    ui->tableWidget_outers->setRowCount(0);

    if (el->id>-1)
        ui->lineEdit_id_element->setText(QString::number(el->id));

    ui->textEdit_error->clear();

    if (!element->script_error.isEmpty())
    {
        //если имеется ошибка в скрипте то выводим текст
        ui->textEdit_error->setText(element->script_error);
    }

//запрещаем редактирование если это не создание новой функции
    if (el->function.id!=-1){
        ui->tableWidget_parameters_in->setEnabled(false);
        ui->tableWidget_parameters_out->setEnabled(false);
       // ui->tableWidget_outers->set(false);
        ui->comboBox_groupe->setEnabled(false);
        ui->lineEdit_FunctionName->setEnabled(false);
        ui->pushButton_append_parameter_connectors_out->setEnabled(false);
        ui->pushButton_append_parameter_in->setEnabled(false);
        ui->pushButton_append_parameter_out->setEnabled(false);
        ui->pushButton_delete_parameter_connectors_out->setEnabled(false);
        ui->pushButton_delete_parameter_in->setEnabled(false);
        ui->pushButton_delete_parameter_out->setEnabled(false);
    }
    else{
        ui->tableWidget_parameters_in->setEnabled(true);
        ui->tableWidget_parameters_out->setEnabled(true);
        ui->tableWidget_outers->setEnabled(true);
        ui->comboBox_groupe->setEnabled(true);
        ui->lineEdit_FunctionName->setEnabled(true);
        ui->pushButton_append_parameter_connectors_out->setEnabled(true);
        ui->pushButton_append_parameter_in->setEnabled(true);
        ui->pushButton_append_parameter_out->setEnabled(true);
        ui->pushButton_delete_parameter_connectors_out->setEnabled(true);
        ui->pushButton_delete_parameter_in->setEnabled(true);
        ui->pushButton_delete_parameter_out->setEnabled(true);
    }


    ui->lineEdit_FunctionName->setText(el->function.comand_name);

    foreach (Parameter parameter, el->parameters) {
        if (parameter.inout_type == 1 && parameter.parameters_name == "Выход")
            continue;
        //заполняем входные параметры
        if (parameter.inout_type == 0 && !parameter.parameters_type.contains("connector"))
        {
            AppendParameter(ui->tableWidget_parameters_in, parameter,  "a"+QString::number(ui->tableWidget_parameters_in->rowCount())+"");
        }

        //заполняем выходы
        if (parameter.inout_type == 1 && parameter.parameters_type.contains("connector_out"))
        {
            AppendParameter(ui->tableWidget_outers, parameter,  "out[0]");
        }

        //заполняем выходные параметры
        if (parameter.inout_type == 1 && !parameter.parameters_type.contains("connector"))
        {
            AppendParameter(ui->tableWidget_parameters_out, parameter,  "out["+QString::number(ui->tableWidget_parameters_out->rowCount()+1)+"]");
        }
    }

    ui->textEdit->setText(el->function.script);
    ui->textEdit_2->setText(el->function.comand_about);
}

void FormEditFunction::appendParameters(QTableWidget *table)
{

    for (int row=0; row<table->rowCount(); row++) {
        Parameter parameter;
        parameter.parameters_name = table->item(row,0)->text();
        parameter.parameters_type = table->item(row,1)->text();
        parameter.parameter_type = table->item(row,2)->text();
        parameter.parameter_name = table->item(row,3)->text();

        parameter.id_element_connect = -1;
        parameter.id = -1;
        parameter.comand_index = -1;
        parameter.is_delete = true;
        parameter.parameter_number = 0;
        if (table == ui->tableWidget_parameters_in)
            parameter.inout_type = 0;
        else
            parameter.inout_type = 1;
        element->parameters.append(parameter);
    }


    if (ui->tableWidget_outers->rowCount()==0) //если нет никакого другого выхода из функции то добавляем по умолчанию
        DataBase::AppendOuterParameter(element->parameters , 1);

}

//нажатие на кнопку сохранить
void FormEditFunction::on_pushButton_clicked()
{
    element->function.script = ui->textEdit->toPlainText();
    element->function.comand_about = ui->textEdit_2->toPlainText();
    element->function.comand_name = ui->lineEdit_FunctionName->text();

    element->service.service_name = ui->comboBox_groupe->currentText();



    if (element->function.id==-1){ //добавляем новую функцию в БД

        element->parameters.clear();

        DataBase::AppendOuterParameter(element->parameters , 0);

        appendParameters(ui->tableWidget_parameters_in);
        appendParameters(ui->tableWidget_parameters_out);
        appendParameters(ui->tableWidget_outers);

        emit sig_save_function_to_bd(element);
    }
    else{
        emit sig_change_function_element(element); //меняем только текст функции
    }

    element = 0;

    ui->textEdit->clear();
    this->close();
}

void FormEditFunction::on_pushButton_2_clicked()
{
    ui->textEdit->clear();
    this->close();
}

void FormEditFunction::on_pushButton_append_parameter_in_clicked()
{
    Parameter parameter;
    parameter.parameters_name = "Значение"+QString::number(ui->tableWidget_parameters_in->rowCount());
    parameter.parameters_type = "number";
    parameter.parameter_type = "";
    parameter.parameter_name = "";

    parameter.id_element_connect = -1;
    parameter.id = -1;
    parameter.comand_index = -1;
    parameter.is_delete = true;
    parameter.parameter_number = 0;
    parameter.inout_type = 0;

    AppendParameter(ui->tableWidget_parameters_in, parameter,  "a"+QString::number(ui->tableWidget_parameters_in->rowCount())+"");
}

void FormEditFunction::on_pushButton_delete_parameter_in_clicked()
{
    ui->tableWidget_parameters_in->removeRow(ui->tableWidget_parameters_in->currentRow());
}

void FormEditFunction::on_pushButton_append_parameter_out_clicked()
{
    Parameter parameter;
    parameter.parameters_name = "Результат"+QString::number(ui->tableWidget_parameters_out->rowCount());
    parameter.parameters_type = "number";
    parameter.parameter_type = "";
    parameter.parameter_name = "";

    parameter.id_element_connect = -1;
    parameter.id = -1;
    parameter.comand_index = -1;
    parameter.is_delete = true;
    parameter.parameter_number = 0;
    parameter.inout_type = 1;
    AppendParameter(ui->tableWidget_parameters_out, parameter,  "out["+QString::number(ui->tableWidget_parameters_out->rowCount()+1)+"]");
}

void FormEditFunction::on_pushButton_delete_parameter_out_clicked()
{
    ui->tableWidget_parameters_out->removeRow(ui->tableWidget_parameters_out->currentRow());
}

void FormEditFunction::on_pushButton_append_parameter_connectors_out_clicked()
{
    Parameter parameter;
    parameter.parameters_name = "Выход"+QString::number(ui->tableWidget_outers->rowCount());
    parameter.parameters_type = "connector_out";
    parameter.parameter_type = "connector_out";
    parameter.parameter_name = "";

    parameter.id_element_connect = -1;
    parameter.id = -1;
    parameter.comand_index = -1;
    parameter.is_delete = true;
    parameter.parameter_number = 0;
    parameter.inout_type = 1;
    AppendParameter(ui->tableWidget_outers, parameter,  "out[0]");
}

void FormEditFunction::on_pushButton_delete_parameter_connectors_out_clicked()
{
    if (ui->tableWidget_outers->rowCount()>1) //запрещаем удалять все выходы, должен оставаться хотябы один
    ui->tableWidget_outers->removeRow(ui->tableWidget_outers->currentRow());
}
