#ifndef SCRIPTWORKERMANAGER_H
#define SCRIPTWORKERMANAGER_H

#include <QObject>
#include "Structures.h"
#include "Scenariy_Worker.h"

class DataBase;

class ScriptWorkerManager : public QObject
{
    Q_OBJECT
public:
    explicit ScriptWorkerManager(QObject *parent = 0);

signals:
     void sig_StartScript(Script *,int);
     void sig_state(Script*,int,QString,int);

public slots:

    void slot_StartScript(Script *,int);
    void slot_StopScript(Script *);
    void slot_load_script(Script *script, QString name,int id_start,int tek_element_id);

private:
    QMap <Script *,ScriptWorker *> scripts;
};

#endif // SCRIPTWORKERMANAGER_H
