#ifndef GLOBALVALUES_H
#define GLOBALVALUES_H

#include <QObject>
#include <QMap>
#include "Structures.h"



class GlobalValues : public QObject
{
    Q_OBJECT
public:
    explicit GlobalValues(QObject *parent = 0);
    static QList <Parameter *> Values;
signals:

public slots:
};

#endif // GLOBALVALUES_H
