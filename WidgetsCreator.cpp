#include "WidgetsCreator.h"

WidgwtsCreator::WidgwtsCreator(QObject *parent) : QObject(parent)
{



    formScriptsRedactor =new FormScriptRedactor;

    QSettings settings(QCoreApplication::applicationDirPath()+"/settings.ini",QSettings::IniFormat);
    settings.beginGroup("DataBase");
    QString bd_type = settings.value("Type").toString(); //"SQLITE" or "POSTGRESS"
    QString bd_name = settings.value("Name").toString();
    QString bd_username = settings.value("UserName").toString();
    QString bd_password = settings.value("Password").toString();
    QString bd_IP = settings.value("IP").toString();
    int bd_port = settings.value("Port").toInt();
    settings.endGroup();




    //connect(act_techplan, SIGNAL(triggered(bool)),redactor, SLOT(show()), Qt::QueuedConnection);

    //поток выполняющий сценарий
    scriptWorkerManager = new ScriptWorkerManager;

    connect(scriptWorkerManager, SIGNAL(sig_state(Script*,int,QString,int)),formScriptsRedactor->view_script_redactor, SLOT(slot_state(Script*,int,QString,int)), Qt::QueuedConnection);
    connect(formScriptsRedactor->view_script_redactor, SIGNAL(sig_start_script(Script*,int)),scriptWorkerManager, SLOT(slot_StartScript(Script*,int)), Qt::QueuedConnection);
    connect(formScriptsRedactor->view_script_redactor, SIGNAL(sig_stop_script(Script*)),scriptWorkerManager, SLOT(slot_StopScript(Script*)), Qt::QueuedConnection);



    bd=new DataBase;
    connect(this,SIGNAL(sig_reconnect_BD(QString , QString ,QString ,int ,QString , QString )),bd,SLOT(slot_reconnect(QString , QString ,QString ,int ,QString , QString )),Qt::QueuedConnection);
    connect(bd,SIGNAL(sig_bd_open(bool)),formScriptsRedactor->view_script_redactor,SLOT(slot_bd_open(bool)),Qt::QueuedConnection);
    connect(bd,SIGNAL(sig_bd_scripts_loaded()),formScriptsRedactor,SLOT(slot_bd_script_loaded()),Qt::QueuedConnection);
    connect(bd,SIGNAL(sig_bd_open(bool)),formScriptsRedactor->view_script_redactor,SLOT(slot_updateWidgetFunctions()),Qt::QueuedConnection);
    connect(bd,SIGNAL(sig_read_global_values()),formScriptsRedactor,SLOT(slot_append_global_values_to_form()),Qt::QueuedConnection);
    connect(bd,SIGNAL(sig_scripts_read_from_bd(QMap<QString,Script*>)),formScriptsRedactor->view_script_redactor,SLOT(slot_updateWidgetFunctions()),Qt::QueuedConnection);

    //connect(bd,SIGNAL(sig_script_read_from_bd(Script*)),widgetsScripts,SLOT(slot_script_read_from_bd(Script*)),Qt::QueuedConnection);

    connect(bd,SIGNAL(sig_bd_open(bool)),formScriptsRedactor->formScripts,SLOT(slot_bd_open(bool)),Qt::QueuedConnection);
    connect(formScriptsRedactor,SIGNAL(sig_save()),bd,SLOT(slot_save()),Qt::QueuedConnection);





    connect(bd, SIGNAL(sig_updateWidgetFunctions()),formScriptsRedactor, SLOT(slot_updateWidgetFunctions()), Qt::QueuedConnection);
    connect(bd,SIGNAL(sig_bd_open(bool)),formScriptsRedactor,SLOT( slot_updateWidgetFunctions()),Qt::QueuedConnection);

    connect(formScriptsRedactor->view_script_redactor, SIGNAL(sig_element_add_bd(ScriptElement *)),bd, SLOT(slot_element_add(ScriptElement *)), Qt::QueuedConnection);
    connect(bd, SIGNAL(sig_set_id_last(int)),formScriptsRedactor, SLOT(slot_set_id_last(int)), Qt::QueuedConnection);
    connect(formScriptsRedactor->formScripts, SIGNAL(sig_script_selected(int,QString,QString)),formScriptsRedactor->view_script_redactor, SLOT(slot_script_selected(int,QString,QString)), Qt::QueuedConnection);
    //connect(formScriptsRedactor->view_script_redactor, SIGNAL(sig_element_set_pos(int,QPoint)),bd, SLOT(slot_element_set_pos(int,QPoint)), Qt::QueuedConnection);
    //connect(formScriptsRedactor->view_script_redactor, SIGNAL(sig_element_change(ScriptElement *)),bd, SLOT(slot_element_change(ScriptElement *)), Qt::QueuedConnection);
    //connect(formScriptsRedactor->view_script_redactor, SIGNAL(sig_pin_connector_add(ScriptElement *,int )),bd, SLOT(slot_changeParameterValue(ScriptElement *,int )), Qt::QueuedConnection);
    //connect(formScriptsRedactor->view_script_redactor, SIGNAL(sig_remove_pin(int,QString)),bd, SLOT(slot_remove_pin(int,QString)), Qt::QueuedConnection);
    connect(formScriptsRedactor->view_script_redactor->formEditFunction, SIGNAL(sig_save_function_to_bd(ScriptElement*)),bd, SLOT(slot_save_function_to_bd(ScriptElement*)), Qt::QueuedConnection);
    connect(formScriptsRedactor->view_script_redactor->formEditFunction, SIGNAL(sig_change_function_element(ScriptElement*)),bd, SLOT(slot_element_change(ScriptElement*)), Qt::QueuedConnection);

    //connect(&formScriptsRedactor->view_script_redactor->formEditFunction, SIGNAL(sig_set_function_text(int,QString)),bd, SLOT(slot_set_function_text(int,QString)), Qt::QueuedConnection);
    //connect(formScriptsRedactor->listWidgetParamreters, SIGNAL(sig_changeParameterValue(ScriptElement*,int)),bd, SLOT(slot_changeParameterValue(ScriptElement*,int)), Qt::QueuedConnection);




    timer_plan_control=new QTimer;
    connect(timer_plan_control,SIGNAL(timeout()),SLOT(slot_plan_control()),Qt::QueuedConnection); //таймер отслеживающий план
    timer_script_start=new QTimer;
    connect(timer_script_start,SIGNAL(timeout()),SLOT(slot_script_start()),Qt::QueuedConnection); //таймер отслеживающий план


    emit sig_reconnect_BD(bd_type,bd_IP,bd_name,bd_port,bd_username,bd_password);


    formScriptsRedactor->showMaximized();

}
