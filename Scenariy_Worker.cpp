#include "Scenariy_Worker.h"
#include "GlobalValues.h"
#include "DataBase.h"

ScriptWorker::ScriptWorker( QObject *parent) : QThread(parent)
{
    this->moveToThread(this);
    exit=false;
}

void ScriptWorker::setScript(Script *script_, int id_start_element_)
{
    script = script_;
    id_start_element = id_start_element_;
}


void ScriptWorker::run()
{
    //exec();
    slot_start_script(script,id_start_element);
}


//--------- перевод значений входящих переменных в зависимости от типа
void ScriptWorker::AppendValue(QScriptValueList &list, QList <QVariant> &list_variant,Parameter* var)
{
    if (var->parameters_type.toLower()=="number"){
        //цифровые переводятся в double
        list<<var->value.toDouble(); //берем значение глобальной переменной
        list_variant.append(var->value.toDouble());
    }
    else{
        //с остальными работаем как со строкой
        list<<var->value; //берем значение глобальной переменной
        list_variant.append(var->value);
    }
}


//-------------- запуск сценария с нужного элемента----------------------------
void ScriptWorker::slot_start_script(Script *script,int id_start)
{
    QMap <QString,QVariant> local_values; //локальные переменные в которые происходит запись ответов на  функции

    tek_element = 0;

    if (!script)
        return ;


    QList <int> ids=script->elemens.keys();
    //поиск входного элемента
    if (id_start>0){
        for (int i=0; i<ids.count();i++){
            if (id_start==ids.value(i)){
                tek_element=script->elemens.value(ids.value(i));
                break;
            }
        }
    }
    else {
        // если не нашли начинаем с первого
        foreach (ScriptElement* script, script->elemens) {
            if (script->function.comand_name == "Старт")
            {
                tek_element = script;
            }
        }
       // tek_element=script->elemens.value(ids.value(0));
    }

    bool error_result=false;

    while (!exit && script->is_start && tek_element)
    {
        if (tek_element)
        {
            sig_state(StateFunction::Process,"",tek_element->id); //начало выполнения
            tek_element->script_error="";
            if (tek_element->service.service_identation < DataBase::service_index_function_c){ //если это сервисная функция

                if (tek_element->function.comand_function_id>0){
                    int port_service=10000 + tek_element->service.service_identation * 4;
                    QString ip_service=tek_element->service.server_ip;
                }
            }
            else{
                //если это скриптовая функция
                QScriptEngine engine;


                int count_parameters=0;
                QString function_temp = "(function("; //функцию qtsqript собираем по кусочкам
                QScriptValueList list;
                QList <QVariant> list_variant; //для вспомогательных функций
                //проходим по параметрам элемента и ищем подключения для чтения входных значений
                foreach (Parameter parameter, tek_element->parameters) {
                    //пин соединен с другим элементом и это соединение с параметром
                    if (parameter.inout_type == 0 && !parameter.parameters_type.contains("connector") && parameter.id_element_connect!=-1){
                        //если приконекчен к чему либо
                        ScriptElement *element_connected = script->elemens.value(parameter.id_element_connect);
                        if (element_connected){
                            //проходим по соединенному элементу и читаем значения
                            foreach (Parameter par, element_connected->parameters) {
                                if (par.parameters_name == parameter.parameters_name_connect){
                                    if (element_connected->function.comand_name == "Получить")
                                        //если подключены к глобальным переменным
                                    {
                                        foreach (Parameter* var, GlobalValues::Values) {
                                            if (var->parameters_name == par.parameters_name)
                                            {
                                                AppendValue(list, list_variant, var);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //берем значение у подключенного пина
                                        AppendValue(list, list_variant,  &par);
                                    }
                                    function_temp = function_temp + "a"+QString::number(count_parameters);
                                    count_parameters++;
                                }
                            }
                        }
                    }
                    else if (parameter.inout_type == 0 && !parameter.parameters_type.contains("connector")){
                        //иначе берем заданный входной параметр
                        AppendValue(list, list_variant, &parameter);
                        function_temp =function_temp + "a"+QString::number(count_parameters);
                        count_parameters++;
                    }
                }



                if(tek_element->function.comand_name=="Пауза")
                {
                    //ставит поток на паузу в течении заданного во входных параметрах времени в мс
                    bool ok = false;
                    int delay = list_variant.value(0).toInt(&ok);
                    if (ok)
                        msleep(delay);
                }

                if(tek_element->function.comand_name=="Скрипт")
                {
                    //останавливает выполнение скрипта и запускает другой скрипт с заданного элемента
                    bool ok = false;
                    QString name = list_variant.value(0).toString();
                    int id_start = list_variant.value(1).toInt(&ok);
                    if (!name.isEmpty() && ok){
                        emit sig_load_script(script, name,id_start,tek_element->id);
                        exit = true;
                        continue;
                    }
                }


                function_temp = function_temp + ") { out = Array; ";

                //инициализируем выходной массив скрипта
                int out_count = 1;
                foreach (Parameter par, tek_element->parameters) {
                    if (par.inout_type == 1 && !par.parameters_type.contains("connector")){
                        //ставим выход по умолчанию
                        function_temp = function_temp + " out["+QString::number(out_count)+"]=0; out[0]=\"Выход\";";
                        out_count++;
                    }
                }

                function_temp = function_temp +tek_element->function.script+"; return out;})";

                QScriptValue object = engine.evaluate(function_temp);
                QScriptValue result = object.call(object, list);

                QStringList out;
                QString outer="Выход"; //выход по умолчанию
                QString rez;
                if( result.isError() ) {

                    rez = result.toString() ;
                    tek_element->script_error = rez;
                    //ошибки выполнения скрипта выводим в окно коррекции функци
                    sig_state(script,StateFunction::EndError,rez,tek_element->id);
                }
                else{
                    sig_state(script,StateFunction::EndOk,"",tek_element->id); //завершено успешно
                    QScriptValueIterator it(result);
                    while(it.hasNext()){
                        //получаем результаты работы скрипта
                        it.next();
                        if (!(it.flags() && QScriptValue::SkipInEnumeration))
                        {
                            int parameter_pos = it.name().toInt();
                            if (parameter_pos==0){ //какой выход используем
                                QString exit_name = it.value().toString();
                                outer = exit_name;
                            }
                            else if (parameter_pos<tek_element->parameters.count())
                            {
                                out.append(it.value().toString()); //считываем выходные параметры они все как строковые
                            }
                        }
                    }
                }

                int num = 0;
                int out_value = 0;
                int next_element = -1; //следующий элемент сценария к которому подключен выход
                foreach (Parameter par, tek_element->parameters) {
                    //записываем по порядку в выходные параметры
                    if (par.inout_type == 1 && !par.parameters_type.contains("connector")){
                        if (par.id_element_connect!=-1)
                        {
                            foreach (Parameter *var, GlobalValues::Values) {
                                if (var->parameters_name == par.parameters_name_connect)
                                {
                                    var->value = out.value(out_value);
                                }
                            }
                        }
                        else
                        {

                            tek_element->parameters[num].value = out.value(out_value);

                        }

                        out_value++;

                    }



                    //ищем на какой выход будет идти сценарий
                    if (par.parameters_type.contains("connector_out") && outer == tek_element->parameters[num].parameters_name)
                    {
                        if (par.id_element_connect!=-1){
                            next_element = par.id_element_connect;
                        }
                    }
                    num++;
                }
                if (next_element!=-1) //если есть следующий элемент
                {
                    tek_element=script->elemens.value(next_element);
                }
                else
                {
                    //завершаем выполнение скрипта
                    sig_state(script,StateFunction::Finish,"",0); //завершено успешно
                    exit=true;
                    break;
                }

            }


        }
    }

    sig_state(script,StateFunction::Finish,"",0); //завершено успешно

}




