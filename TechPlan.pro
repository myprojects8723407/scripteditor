#-------------------------------------------------
#
# Project created by QtCreator 2021-12-06T09:38:02
#
#-------------------------------------------------

QT       += core gui sql network script

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

DESTDIR = ../SqriptEditor

TARGET = SqriptEditor
TEMPLATE = app



# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

#INCLUDEPATH += ../../libs
#include(../../libs/libs.pri)




unix{
    DEFINES +=ASTRA
 #   INCLUDEPATH += ../libs
#    include(../libs/libs.pri)
#    linux: LIBS += -luuid
}

DISTFILES +=

RESOURCES += \
    resurses.qrc

FORMS += \
    forms/FormAddScenariy.ui \
    forms/FormAppendToPlan.ui \
    forms/FormDataBase.ui \
    forms/FormEditFunction.ui \
    forms/FormRedactor.ui \
    forms/FormScriptRedactor.ui \
    forms/FormTreeViewFunctins.ui

HEADERS += \
    forms/FormAddRemoveScripts.h \
    forms/FormAddScenariy.h \
    forms/FormDataBase.h \
    forms/FormEditFunction.h \
    forms/FormScriptsRedactor.h \
    forms/FormTreeViewFunctions.h \
    forms/ViewScriptRedaktor.h \
    models/MyComboDelegate.h \
    models/MyComboModel.h \
    models/TableModelScripts.h \
    DataBase.h \
    GlobalValues.h \
    Scenariy_Worker.h \
    ScriptWorkerManager.h \
    Structures.h \
    tableWidgetParamreters.h \
    WidgetsCreator.h \
    graphicsItems/G_Function.h \
    graphicsItems/G_FunctionsManager.h \
    graphicsItems/G_Lineconnector.h \
    graphicsItems/G_Pin.h

SOURCES += \
    forms/FormAddRemoveScripts.cpp \
    forms/FormAddScenariy.cpp \
    forms/FormDataBase.cpp \
    forms/FormEditFunction.cpp \
    forms/FormScriptsRedactor.cpp \
    forms/FormTreeViewFunctins.cpp \
    forms/ViewScriptRedaktor.cpp \
    models/MyComboDelegate.cpp \
    models/MyComboModel.cpp \
    models/TableModelScripts.cpp \
    DataBase.cpp \
    GlobalValues.cpp \
    main.cpp \
    Scenariy_Worker.cpp \
    ScriptWorkerManager.cpp \
    tableWidgetParamreters.cpp \
    WidgetsCreator.cpp \
    graphicsItems/G_Function.cpp \
    graphicsItems/G_Functionsmanager.cpp \
    graphicsItems/G_Lineconnector.cpp \
    graphicsItems/G_Pin.cpp

