#include "TableModelScripts.h"

TableModelScritps::TableModelScritps() : QTableView()
{
    model = 0;
    this->setColumnHidden(0, true);    // Скрываем колонку с id записей
    // Разрешаем выделение строк
    this->setSelectionBehavior(QAbstractItemView::SelectRows);
    // Устанавливаем режим выделения лишь одно строки в таблице
    this->setSelectionMode(QAbstractItemView::SingleSelection);
    // Устанавливаем размер колонок по содержимому
    this->resizeColumnsToContents();
    this->setEditTriggers(QAbstractItemView::NoEditTriggers);
    //this->horizontalHeader()->setStretchLastSection(true);

}

void TableModelScritps::slot_bd_open(bool flag)
{
    if (QSqlDatabase::database("db").isOpen()){

        model = new QSqlTableModel(0,QSqlDatabase::database("db"));
        this->setModel(model);     // Устанавливаем модель на TableView
        model->setTable("scripts");

        model->setHeaderData(0,Qt::Horizontal,"ИД");
        model->setHeaderData(1,Qt::Horizontal,"Название");
        model->setHeaderData(2,Qt::Horizontal,"Коментарий");
        this->setColumnHidden(0, true);

        // Устанавливаем сортировку по возрастанию данных по нулевой колонке
        model->setSort(0,Qt::AscendingOrder);
        model->select(); // Делаем выборку данных из таблицы
    }

}

