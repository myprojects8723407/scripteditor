#ifndef MYCOMBODELEGATE_H
#define MYCOMBODELEGATE_H

#include <QObject>
#include <QComboBox>
#include <QItemDelegate>
#include "MyComboModel.h"


class MyComboDelegate : public QItemDelegate
{
    Q_OBJECT
public:

    MyComboModel *model;
    explicit MyComboDelegate(QString, QString, QString,QObject *parent = 0);

    QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const;
    void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& index) const;

signals:

public slots:
};

#endif // MYCOMBODELEGATE_H
