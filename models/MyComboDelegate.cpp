#include "MyComboDelegate.h"

MyComboDelegate::MyComboDelegate(QString table_name,QString id_name,QString data_name,QObject *parent) : QItemDelegate(parent)
{
    model=new MyComboModel(table_name, id_name, data_name);
}

QWidget *MyComboDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QComboBox* comboBox = new QComboBox(parent);

    comboBox->setModel(model);

    return comboBox;
}



void MyComboDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QString value = index.model()->data(index, Qt::DisplayRole).toString().simplified();
    QComboBox *comboBox = static_cast<QComboBox*>(editor);
    comboBox->addItem(value);
}

void MyComboDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QComboBox *comboBox = static_cast<QComboBox*>(editor);
    QString type = comboBox->itemData(comboBox->currentIndex(), Qt::DisplayRole ).toString().simplified();
    model->setData(index, type, Qt::EditRole);

}

void MyComboDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &/*index*/) const
{
    editor->setGeometry(option.rect);
}
