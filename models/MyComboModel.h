#ifndef MYCOMBOMODEL_H
#define MYCOMBOMODEL_H

#include <QObject>
#include <QSqlQueryModel>
#include <QSqlDatabase>
#include <QSqlQuery>




class MyComboModel : public QSqlQueryModel
{
    Q_OBJECT

    QVariant dataFromParent(QModelIndex index, int column) const;

public:

    enum Columns // Depends with 'query.prepare( QString( "SELECT ... '
    {
        Id,
        Data,
    };

    QString sql;

    explicit MyComboModel( QString, QString,QString,QObject *parent = 0 );
    void slot_select(QString parameter_name, int id );
    void change_data();
    virtual QVariant data(const QModelIndex &item, int role = Qt::DisplayRole) const;
    virtual int rowCount(const QModelIndex &parent) const;

};

#endif // MYCOMBOMODEL_H
