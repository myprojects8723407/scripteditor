#ifndef TABLEMODELSCENARIY_H
#define TABLEMODELSCENARIY_H

#include <QObject>
#include <QSqlTableModel>
#include <QTableView>


class TableModelScritps : public QTableView
{
    Q_OBJECT
public:
    explicit TableModelScritps();
    void setupModel(const QString &tableName, const QStringList &headers);
    QSqlTableModel  *model;


signals:

public slots:
    void slot_bd_open(bool);

private:

};

#endif // TABLEMODELSCENARIY_H
