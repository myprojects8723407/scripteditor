#include "MyComboModel.h"

#include <QFile>

MyComboModel::MyComboModel(QString table_name,QString id_name,QString data_name,  QObject *parent ) :
    QSqlQueryModel( parent )
{
    //QSqlQuery query(QSqlDatabase::database("db"));

   /* sql="SELECT "+id_name;
    if (id_name.isEmpty())
        sql="SELECT * ";

    if (!data_name.isEmpty())
        sql=sql + ","+data_name;*/

     sql="SELECT "+id_name+","+data_name+" FROM "+table_name;

//    sql=sql +" FROM "+table_name;
//    query.prepare(sql);
   change_data();
}

void MyComboModel::slot_select(QString parameter_name, int id )
{
    QSqlQuery query(QSqlDatabase::database("db"));
    QString sql_new=sql+" WHERE "+parameter_name+"="+QString::number(id);
    query.prepare(sql_new);
    bool rez= query.exec();
    QSqlQueryModel::setQuery( query );
}

void MyComboModel::change_data()
{
    QSqlQuery query(QSqlDatabase::database("db"));
    query.prepare(sql);
    bool rez= query.exec();
    QSqlQueryModel::setQuery( query );
}

QVariant MyComboModel::dataFromParent( QModelIndex index, int column ) const
{
    return QSqlQueryModel::data( QSqlQueryModel::index( index.row() , column ) );
}

int MyComboModel::rowCount(const QModelIndex &parent) const
{
    return QSqlQueryModel::rowCount( parent ); // Add info about first empty row
}

QVariant MyComboModel::data(const QModelIndex & item, int role /* = Qt::DisplayRole */) const
{
    QVariant result;

        switch( role )
        {
            case Qt::UserRole:
                result = dataFromParent( item, Id );
                break;
            case Qt::DisplayRole:
                result = dataFromParent( item, Data );
                break;
            case Qt::EditRole:
                result = dataFromParent( item, Data );
                break;
            default:
                break;
        }


    return result;
}
